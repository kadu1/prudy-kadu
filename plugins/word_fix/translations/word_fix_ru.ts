<?xml version="1.0" ?><!DOCTYPE TS><TS language="ru" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>разговор</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>коррекция слова</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>позволяют коррекции слова</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>Слово в замене</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>значение, при котором изменение</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>изменения</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>удалить</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>слово</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Заменить</translation>
    </message>
</context>
</TS>