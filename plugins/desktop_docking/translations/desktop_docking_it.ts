<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Barra</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Desktop MInimizzato</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Posizione Orizzontale</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Posizione verticale</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Trasparente</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Colore di sfondo</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Abilita lo spostamento ingressi nel menu minimizzato</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Sposta</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Abilita spostamento icone sul desktop</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista contatti</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Sposta</translation>
    </message>
</context>
</TS>