<?xml version="1.0" ?><!DOCTYPE TS><TS language="es_ES" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Bandeja del sistema</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Desktop Docking</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Posición horizontal</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Posición vertical</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Transparente</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Color de fondo</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Habilitar Mueva la entrada en el menú de docklet</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Mover</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Habilitar el icono en movimiento en el escritorio. Tras pulsar mueve el cursor sobre el icono de conexión y luego muevelo. Pulse cualquier tecla del ratón cuando el icono está en su lugar correcto.</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista de amigos</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Mover</translation>
    </message>
</context>
</TS>