<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Tray</translation>
    </message>
    <message>
        <source>Desktop Docking</source>
        <translation>Desktop Docking</translation>
    </message>
    <message>
        <source>Horizontal position</source>
        <translation>Horizontal position</translation>
    </message>
    <message>
        <source>Vertical position</source>
        <translation>Vertical position</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation>Transparent</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Background color</translation>
    </message>
    <message>
        <source>Enable Move entry in docklet&apos;s menu</source>
        <translation>Enable Move entry in docklet&apos;s menu</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Move</translation>
    </message>
    <message>
        <source>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</source>
        <translation>Enable icon moving on desktop. After pressing move cursor over docking icon and then move. Press any mouse key when the icon is in right place.</translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Buddies list</translation>
    </message>
</context>
<context>
    <name>DesktopDock</name>
    <message>
        <source>Move</source>
        <translation>Move</translation>
    </message>
</context>
</TS>