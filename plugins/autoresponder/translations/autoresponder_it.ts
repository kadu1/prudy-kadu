<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Configurazione</translation>
    </message>
    <message>
        <source>Only for the first time</source>
        <translation>Rispondi solo una volta</translation>
    </message>
    <message>
        <source>Respond to conferences</source>
        <translation>Rispondi nella conferenza</translation>
    </message>
    <message>
        <source>Status invisible</source>
        <translation>Non presente</translation>
    </message>
    <message>
        <source>Status busy</source>
        <translation>Occupato</translation>
    </message>
    <message>
        <source>Status available</source>
        <translation>Accessibile</translation>
    </message>
    <message>
        <source>Auto Responder</source>
        <translation>Risposta automatica</translation>
    </message>
    <message>
        <source>Auto answer text</source>
        <translation>Auto risposta</translation>
    </message>
    <message>
        <source>Choose Status</source>
        <translation>Scegli stato</translation>
    </message>
</context>
<context>
    <name>AutoResponder</name>
    <message>
        <source>KADU AUTORESPONDER:</source>
        <translation>RISPOSTA AUTOMATICA:</translation>
    </message>
</context>
<context>
    <name>AutoresponderConfigurator</name>
    <message>
        <source>I am busy.</source>
        <translation>Sono occupato.</translation>
    </message>
</context>
</TS>