<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation>Pojedyncze okno</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation>Pozycja listy kontaktów</translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Wybierz pozycję listy kontaktów w trybie pojedynczego okna</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Strona lewa</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Strona prawa</translation>
    </message>
    <message>
        <source>Show number of messages on tab</source>
        <translation>Wyświetl liczbę wiadomości na zakładce</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Skróty</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Przełącz na poprzednią zakładkę</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Przełącz na następną zakładkę</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation>Pokaż/ukryj listę kontaktów</translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation>Przełącz focus pomiędzy listą kontaktów a zakładkami</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Zachowanie</translation>
    </message>
</context>
<context>
    <name>SingleWindow</name>
    <message>
        <source>Conference [%1]</source>
        <translation>Konferencja [%1]</translation>
    </message>
</context>
</TS>