<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation>SingleWindow</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation>Roster position</translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Choose position of roster in Single Window mode</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <source>Show number of messages on tab</source>
        <translation>Show number of messages on tab</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Switch to previous tab</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Switch to next tab</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation>Show/hide roster</translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation>Switch focus between roster and tabs</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Behaviour</translation>
    </message>
</context>
<context>
    <name>SingleWindow</name>
    <message>
        <source>Conference [%1]</source>
        <translation>Conference [%1]</translation>
    </message>
</context>
</TS>