<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation> Fenêtre unique</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation>Position de Roster</translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Choisir la position de roster en mode Fenêtre unique</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <source>Show number of messages on tab</source>
        <translation>Afficher le nombre de messages dans l&apos;onglet</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Aller à l&apos;onglet précédent</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Aller à l&apos;onglet suivant</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation>Afficher / masquer roster</translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation>Basculer le focus entre roster et les onglets</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SingleWindow</name>
    <message>
        <source>Conference [%1]</source>
        <translation>Conférence [%1]</translation>
    </message>
</context>
</TS>