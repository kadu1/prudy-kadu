<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.0">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>Arkadaşların bilgilerini &amp;gösterir</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Arkadaşların Bilgileri</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Arkadaş</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protokol</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Takma İsim</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <source>Domain name</source>
        <translation>Domain Adı</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Tanım</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Ülke</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>En son görüldüğü zaman</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Kapat</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>