<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>&amp;Show infos about buddies</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Buddies Information</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Buddy</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protocol</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <source>Domain name</source>
        <translation>Domain name</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>State</source>
        <translation>State</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>Last time seen on</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
</context>
</TS>