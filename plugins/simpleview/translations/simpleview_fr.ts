<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Keep contact list size</source>
        <translation>Conserver la taille de la liste de contact</translation>
    </message>
    <message>
        <source>Hide scroll bar</source>
        <translation>Masquer la barre de défilement</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Simple View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Hide window borders</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SimpleView</name>
    <message>
        <source>Simple view</source>
        <translation>Vue simplifiée</translation>
    </message>
</context>
</TS>