<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Přehrávač zvuku</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Přehrávač</translation>
    </message>
</context>
</TS>