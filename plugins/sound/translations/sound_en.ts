<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Sound</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Sound theme</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Sound paths</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Test sound playing</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Play a sound</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation>Enable sound notifications</translation>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation>Play Sounds</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Custom</translation>
    </message>
</context>
</TS>