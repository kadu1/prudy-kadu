<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Son</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Thème sonore</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Emplacement des sons</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Test sonore</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Jouer un son</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation>Jouer les sons</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
</context>
</TS>