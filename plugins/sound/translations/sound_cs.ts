<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Zvukové téma</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Cesta ke zvukům</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Vyzkoušet přehrávání zvuku</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Přehrát zvuk</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation>Povolit zvuková oznámení</translation>
    </message>
</context>
<context>
    <name>SoundActions</name>
    <message>
        <source>Play Sounds</source>
        <translation>Přehrát zvuky</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
</context>
</TS>