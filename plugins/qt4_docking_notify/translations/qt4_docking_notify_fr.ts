<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <source>Tray Icon Balloon</source>
        <translation>Icône de Balloon dans la barre de notification</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Temps échu</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n seconde</numerusform><numerusform>%n secondes</numerusform></translation>
    </message>
    <message>
        <source>Notification icon</source>
        <translation>Icône de notification</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Informations</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation>Critique</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntaxe</translation>
    </message>
    <message>
        <source>No Icon</source>
        <translation>Aucune icône</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Qt4NotifyConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Configurer</translation>
    </message>
    <message>
        <source>Tray icon balloon&apos;s look configuration</source>
        <translation>Configurer l&apos;apparence de l&apos;icône de Balloon dans la barre de notification</translation>
    </message>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - titre (p. ex. Nouveau message) %&amp;m - notification texte (p. ex. Message de Jim), %&amp;d - details (p. ex. message citation),
%&amp;i - notification icône</translation>
    </message>
</context>
</TS>