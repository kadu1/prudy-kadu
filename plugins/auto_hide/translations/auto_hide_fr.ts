<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Temps d&apos;inactivité avant de déclenchement d&apos;Autohide </translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n seconde</numerusform><numerusform>%n secondes</numerusform></translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddies window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Ne pas masquer</translation>
    </message>
</context>
</TS>