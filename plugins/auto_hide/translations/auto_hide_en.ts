<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Autohide idle time</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n second</numerusform><numerusform>%n seconds</numerusform></translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Buddies list</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Buddies window</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Hide buddy list window when unused</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Don&apos;t hide</translation>
    </message>
</context>
</TS>