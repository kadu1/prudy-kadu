<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Unable to connect, server has not been found</source>
        <translation>Unable to connect, server has not been found</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation>Unable to connect</translation>
    </message>
    <message>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</translation>
    </message>
    <message>
        <source>Unable to connect, server has returned unknown data</source>
        <translation>Unable to connect, server has returned unknown data</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during reading</source>
        <translation>Unable to connect, connection break during reading</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during writing</source>
        <translation>Unable to connect, connection break during writing</translation>
    </message>
    <message>
        <source>Unable to connect, invalid password</source>
        <translation>Unable to connect, invalid password</translation>
    </message>
    <message>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation>Unable to connect, error of negotiation TLS</translation>
    </message>
    <message>
        <source>Too many connection attempts with bad password!</source>
        <translation>Too many connection attempts with bad password!</translation>
    </message>
    <message>
        <source>Unable to connect, servers are down</source>
        <translation>Unable to connect, servers are down</translation>
    </message>
    <message>
        <source>Connection broken</source>
        <translation>Connection broken</translation>
    </message>
    <message>
        <source>Connection timeout!</source>
        <translation>Connection timeout!</translation>
    </message>
    <message>
        <source>Disconnection has occurred</source>
        <translation>Disconnection has occurred</translation>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Gadu-Gadu number</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Remember Password</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Add Account</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Forgot Your Password?</translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Change Password</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>E-Mail Address</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Old Password</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation>New Password</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype New Password</source>
        <translation>Retype New Password</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Changing password was successful.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>An error has occurred. Please try again later.</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</translation>
    </message>
</context>
<context>
    <name>GaduChatImageService</name>
    <message>
        <source>This image has %1 KiB and exceeds recommended maximum size of %2 KiB. Some clients may have trouble with too large images.</source>
        <translation>This image has %1 KiB and exceeds recommended maximum size of %2 KiB. Some clients may have trouble with too large images.</translation>
    </message>
    <message>
        <source>Do you really want to send this image?</source>
        <translation>Do you really want to send this image?</translation>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Message too long (%1 &gt;= %2)</source>
        <translation>Message too long (%1 &gt;= %2)</translation>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <source>First Name</source>
        <translation>First Name</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Last Name</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nickname</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Gender</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Birthdate</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>State/Province</source>
        <translation>State/Province</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>IP Address</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>DNS Name</source>
        <translation>DNS Name</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>Protocol Version</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Female</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Male</translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Retype Password</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Remember password</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>E-Mail Address</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Regster Account</source>
        <translation>Regster Account</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</translation>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Delete account</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Gadu-Gadu number</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Remember password</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Forgot Your Password?</translation>
    </message>
    <message>
        <source>Change Your Password</source>
        <translation>Change Your Password</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Account Identity</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Personal info</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Buddies</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connection</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Use default servers</source>
        <translation>Use default servers</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Use encrypted connection</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Remove account</translation>
    </message>
    <message>
        <source>Remove account and unregister from server</source>
        <translation>Remove account and unregister from server</translation>
    </message>
    <message>
        <source>External port</source>
        <translation>External port</translation>
    </message>
    <message>
        <source>You have to compile libgadu with SSL support to be able to enable encrypted connection</source>
        <translation>You have to compile libgadu with SSL support to be able to enable encrypted connection</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Proxy configuration</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <source>Receive images also when I am Invisible</source>
        <translation>Receive images also when I am Invisible</translation>
    </message>
    <message>
        <source>Warn me when the image being sent may be too large</source>
        <translation>Warn me when the image being sent may be too large</translation>
    </message>
    <message>
        <source>Some clients may have trouble with too large images (over 256 KiB).</source>
        <translation>Some clients may have trouble with too large images (over 256 KiB).</translation>
    </message>
    <message>
        <source>Show my status only to buddies on my list</source>
        <translation>Show my status only to buddies on my list</translation>
    </message>
    <message>
        <source>When disabled, anyone can see your status.</source>
        <translation>When disabled, anyone can see your status.</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Enable composing events</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</translation>
    </message>
    <message>
        <source>Block links from anonymous buddies</source>
        <translation>Block links from anonymous buddies</translation>
    </message>
    <message>
        <source>Protects you from potentially malicious links in messages from anonymous buddies</source>
        <translation>Protects you from potentially malicious links in messages from anonymous buddies</translation>
    </message>
    <message>
        <source>Gadu-Gadu Server</source>
        <translation>Gadu-Gadu Server</translation>
    </message>
    <message>
        <source>Custom server IP addresses</source>
        <translation>Custom server IP addresses</translation>
    </message>
    <message>
        <source>Enable file transfers</source>
        <translation>Enable file transfers</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <source>External IP</source>
        <translation>External IP</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Confrim Account Removal</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Are you sure do you want to remove account %1 (%2)?</translation>
    </message>
    <message>
        <source>Status Visibility</source>
        <translation>Status Visibility</translation>
    </message>
    <message>
        <source>You are going to reveal your status to several buddies which are currently not allowed to see it.
Are you sure to allow them to know you are available?</source>
        <translation>You are going to reveal your status to several buddies which are currently not allowed to see it.
Are you sure to allow them to know you are available?</translation>
    </message>
    <message>
        <source>Make my status visible anyway</source>
        <translation>Make my status visible anyway</translation>
    </message>
    <message>
        <source>Stay with private status</source>
        <translation>Stay with private status</translation>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <source>Unknown Gender</source>
        <translation>Unknown Gender</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Male</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Female</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Nick</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <source>Sex</source>
        <translation>Sex</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Family name</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Birth year</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>Family city</source>
        <translation>Family city</translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <source>Gadu-Gadu number:</source>
        <translation>Gadu-Gadu number:</translation>
    </message>
</context>
<context>
    <name>GaduProtocolPlugin</name>
    <message>
        <source>Gadu-Gadu Protocol</source>
        <translation>Gadu-Gadu Protocol</translation>
    </message>
    <message>
        <source>Cannot load Gadu-Gadu Protocol plugin. Please compile libgadu with zlib support.</source>
        <translation>Cannot load Gadu-Gadu Protocol plugin. Please compile libgadu with zlib support.</translation>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <source>Remind password</source>
        <translation>Remind password</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>E-Mail Address</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Send Password</source>
        <translation>Send Password</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error during remind password</source>
        <translation>Error during remind password</translation>
    </message>
    <message>
        <source>Your password has been sent on your email</source>
        <translation>Your password has been sent on your email</translation>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <source>Unregister account</source>
        <translation>Unregister account</translation>
    </message>
    <message>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation>This dialog box allows you to unregister your account. Be aware of using this option.</translation>
    </message>
    <message>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Gadu-Gadu number</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Unregister Account</source>
        <translation>Unregister Account</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation>Unregistation was successful. Now you don&apos;t have any GG number :(</translation>
    </message>
    <message>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation>An error has occurred while unregistration. Please try again later.</translation>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation>Plase wait. New Gadu-Gadu account is being registered.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>An error has occurred during registration. Please try again later.</translation>
    </message>
    <message>
        <source>Registering new Gadu-Gadu account</source>
        <translation>Registering new Gadu-Gadu account</translation>
    </message>
</context>
</TS>