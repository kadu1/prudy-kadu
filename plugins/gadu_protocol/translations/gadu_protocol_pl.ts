<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Unable to connect, server has not been found</source>
        <translation>Połączenie niemożliwe, serwer nie został znaleziony</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation>Połączenie niemożliwe</translation>
    </message>
    <message>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation>Zmień swój e-mail w oknie &quot;Zmień hasło / e-mai&quot;. Pole nowe hasło pozostaw puste.</translation>
    </message>
    <message>
        <source>Unable to connect, server has returned unknown data</source>
        <translation>Połączenie niemożliwe, serwer odpowiedział nieznanymi danymi</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during reading</source>
        <translation>Połączenie niemożliwe, połączenie przerwane w czasie odczytu</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during writing</source>
        <translation>Połączenie niemożliwe, połączenie przerwane w czasie zapisu</translation>
    </message>
    <message>
        <source>Unable to connect, invalid password</source>
        <translation>Połączenie niemożliwe, nieprawidłowe hasło</translation>
    </message>
    <message>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation>Połączenie niemożliwe, błąd negocjacji TLS</translation>
    </message>
    <message>
        <source>Too many connection attempts with bad password!</source>
        <translation>Zbyt wiele połączeń z użyciem błędnego hasła!</translation>
    </message>
    <message>
        <source>Unable to connect, servers are down</source>
        <translation>Połączenie niemożliwe, serwery są niedostępne</translation>
    </message>
    <message>
        <source>Connection broken</source>
        <translation>Zerwane połączenie</translation>
    </message>
    <message>
        <source>Connection timeout!</source>
        <translation>Przekroczenie czasu połączenia!</translation>
    </message>
    <message>
        <source>Disconnection has occurred</source>
        <translation>Nastąpiło rozłączenie</translation>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Dodaj konto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Zapomniałeś hasła?</translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Stare hasło</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź stare hasło.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation>Nowe hasło</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź nowe hasło.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype New Password</source>
        <translation>Powtórz nowe hasło</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Hasło zostało pomyślnie zmienione.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Wystąpił błąd. Proszę spróbować później.</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Błędne dane w wymaganych polach.

Hasło wpisane w polach (&quot;Hasło&quot; i &quot;Powtórz hasło&quot;) muszą być takie same!</translation>
    </message>
</context>
<context>
    <name>GaduChatImageService</name>
    <message>
        <source>This image has %1 KiB and exceeds recommended maximum size of %2 KiB. Some clients may have trouble with too large images.</source>
        <translation>Ten obrazek ma %1 KiB i jest większy niż maksymalny dopuszczalny rozmiar %2 KiB. Niektóre klienty mogą niepoprawnie obsługiwać zbyt duże obrazki.</translation>
    </message>
    <message>
        <source>Do you really want to send this image?</source>
        <translation>Czy na pewno chcesz wysłać ten obrazek?</translation>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Message too long (%1 &gt;= %2)</source>
        <translation>Wiadomość zbyt długa (%1&gt;=%2)</translation>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <source>First Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Data urodzenia</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <source>State/Province</source>
        <translation>Stan/Region</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>DNS Name</source>
        <translation>Nazwa DNS</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>Wersja protokołu</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Powtórz hasło</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Regster Account</source>
        <translation>Zarejestruj konto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Błędne dane w wymaganych polach.

Hasło wpisane w polach (&quot;Hasło&quot; i &quot;Powtórz hasło&quot;) muszą być takie same!</translation>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Zapomniałeś hasła?</translation>
    </message>
    <message>
        <source>Change Your Password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Informacje osobiste</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Znajomi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Połączenie</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <source>Use default servers</source>
        <translation>Użyj domyślnych serwerów</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Używaj szyfrowanego połączenia</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <source>Remove account and unregister from server</source>
        <translation>Usuń konto i wyrejestruj z serwera</translation>
    </message>
    <message>
        <source>External port</source>
        <translation>Zewnętrzny port</translation>
    </message>
    <message>
        <source>You have to compile libgadu with SSL support to be able to enable encrypted connection</source>
        <translation>Libgadu skompilowane ze wsparciem dla SSL jest wymagane aby aktywować szyfrowane połączenie</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Inne</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Konfiguracja proxy</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Obrazki</translation>
    </message>
    <message>
        <source>Receive images also when I am Invisible</source>
        <translation>Odbieraj obrazki także gdy jestem niewidoczny</translation>
    </message>
    <message>
        <source>Warn me when the image being sent may be too large</source>
        <translation>Ostrzeż przed wysyłaniem zbyt dużych obrazków</translation>
    </message>
    <message>
        <source>Some clients may have trouble with too large images (over 256 KiB).</source>
        <translation>Niektóry klienty mogą niepoprawnie obsługiwać zbyt duże obrazki (ponad 256 KiB).</translation>
    </message>
    <message>
        <source>Show my status only to buddies on my list</source>
        <translation>Pokazuj status tylko znajomym</translation>
    </message>
    <message>
        <source>When disabled, anyone can see your status.</source>
        <translation>Gdy nie jest zaznaczone, każdy może oglądać twój status</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Włącz powiadomienia o pisaniu</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Twój rozmówca będzie informowany o tym, że piszesz wiadomość. Działa także w drugą stronę.</translation>
    </message>
    <message>
        <source>Block links from anonymous buddies</source>
        <translation>Blokuj linki od nieznajomych</translation>
    </message>
    <message>
        <source>Protects you from potentially malicious links in messages from anonymous buddies</source>
        <translation>Chroni przed potencjalnie groźnymi linkami w wiadomościach od nieznajomych</translation>
    </message>
    <message>
        <source>Gadu-Gadu Server</source>
        <translation>Serwer Gadu-Gadu</translation>
    </message>
    <message>
        <source>Custom server IP addresses</source>
        <translation>Własne adresy IP serwerów</translation>
    </message>
    <message>
        <source>Enable file transfers</source>
        <translation>Włącz transfery plików</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Sieć</translation>
    </message>
    <message>
        <source>External IP</source>
        <translation>Zewnętrzny adres IP</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Potwierdź usunięcie konta</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Czy na pewno chcesz usunąć konto %1 (%2)?</translation>
    </message>
    <message>
        <source>Status Visibility</source>
        <translation>Widoczność Statusu</translation>
    </message>
    <message>
        <source>You are going to reveal your status to several buddies which are currently not allowed to see it.
Are you sure to allow them to know you are available?</source>
        <translation>Nastąpi ujawnienie statusu niektórym znajomym, którzy aktualnie go nie widzą.
Czy na pewno chcesz im ujawnić swoją dostępność?</translation>
    </message>
    <message>
        <source>Make my status visible anyway</source>
        <translation>Ujawnij mój status mimo wszystko</translation>
    </message>
    <message>
        <source>Stay with private status</source>
        <translation>Uruchom ze statusem widocznym tylko dla znajomych</translation>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <source>Unknown Gender</source>
        <translation>Płeć nieokreślona</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>Sex</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Nazwisko rodzinne</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Rok urodzenia</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miejscowość</translation>
    </message>
    <message>
        <source>Family city</source>
        <translation>Miejscowość rodzinna</translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <source>Gadu-Gadu number:</source>
        <translation>Numer Gadu-Gadu:</translation>
    </message>
</context>
<context>
    <name>GaduProtocolPlugin</name>
    <message>
        <source>Gadu-Gadu Protocol</source>
        <translation>Protokół Gadu-Gadu</translation>
    </message>
    <message>
        <source>Cannot load Gadu-Gadu Protocol plugin. Please compile libgadu with zlib support.</source>
        <translation>Nie można załadować wtyczki Gadu-Gadu. Libgadu ze wsparciem dla zlib jest wymagane.</translation>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <source>Remind password</source>
        <translation>Przypomnij hasło</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Send Password</source>
        <translation>Wyślij hasło</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error during remind password</source>
        <translation>Bład podczas przypominania hasła</translation>
    </message>
    <message>
        <source>Your password has been sent on your email</source>
        <translation>Twoje hasło zostało wysłane na adres e-mailowy</translation>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <source>Unregister account</source>
        <translation>Wyrejestruj konto</translation>
    </message>
    <message>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation>To okno pozwala na wyrejestrowanie konta. Uważaj z używaniem tej opcji.</translation>
    </message>
    <message>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;To okno dialogowe pozwala na trwałe wyrejestrowanie użytkownika z serwera Gadu-Gadu.&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;&lt;font color=&quot;red&quot;&gt;&lt;b&gt;Musisz sobie zdawać sprawę że spowoduje to nieodwracalne usunięcie numeru GG z serwera i nigdy już nie będziesz mógł z niego korzystać!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Unregister Account</source>
        <translation>Wyrejestruj konto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation>Wyrejestrowanie przebiegło prawidłowo. Teraz już nie masz numeru GG :(</translation>
    </message>
    <message>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation>Wystąpił błąd podczas wyrejestrowywania konta. Proszę spróbować później.</translation>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation>Proszę czekać. Nowe konto Gadu-Gadu jest rejestrowane.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation>Rejestracja zakończona. Twój nowy numer to: %1.
Zapisz go wraz z hasłem w bezpiecznym miejscu.
Teraz możesz dodać znajomych do listy kontaktów.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Wystąpił błąd podczas rejestracji. Proszę spróbować później.</translation>
    </message>
    <message>
        <source>Registering new Gadu-Gadu account</source>
        <translation>Rejestrowanie nowego konta Gadu-Gadu</translation>
    </message>
</context>
</TS>