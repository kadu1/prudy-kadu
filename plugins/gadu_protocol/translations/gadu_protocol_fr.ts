<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Unable to connect, server has not been found</source>
        <translation>Connexion impossible. Le serveur est introuvable</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation>Connexion impossible.</translation>
    </message>
    <message>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, server has returned unknown data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, connection break during reading</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, connection break during writing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, invalid password</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Too many connection attempts with bad password!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unable to connect, servers are down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection broken</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection timeout!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Disconnection has occurred</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numéro Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Ajouter un compte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Mot de passe oublié ?</translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Changer de mot de pusse</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresse email</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Ancien mot de passe</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>New Password</source>
        <translation>Nouveau mot de passe</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Retype New Password</source>
        <translation>Répéter le mot de passe</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Caratères</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduChatImageService</name>
    <message>
        <source>This image has %1 KiB and exceeds recommended maximum size of %2 KiB. Some clients may have trouble with too large images.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Do you really want to send this image?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Message too long (%1 &gt;= %2)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>State/Province</source>
        <translation>Pays/Département</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>Adresse IP</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>DNS Name</source>
        <translation>Nom du DNS</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>Version du protocol</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Féminin</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Masculin</translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Répéter le mot de passe</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresse email</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Regster Account</source>
        <translation>Enregistrer un compte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Supprimer le compte</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numéro Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Mot de passe oublié ?</translation>
    </message>
    <message>
        <source>Change Your Password</source>
        <translation>Changer le mot de passe</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Identité du compte</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Informations personnelles</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Amis</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Use default servers</source>
        <translation>Utiliser les serveurs par défaut</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Utiliser une connexion cryptée</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Supprimer un compte</translation>
    </message>
    <message>
        <source>Remove account and unregister from server</source>
        <translation>Supprimer un compte et se désinscrire du serveur</translation>
    </message>
    <message>
        <source>External port</source>
        <translation>Port externe</translation>
    </message>
    <message>
        <source>You have to compile libgadu with SSL support to be able to enable encrypted connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Configuration du proxy</translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Receive images also when I am Invisible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Warn me when the image being sent may be too large</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Some clients may have trouble with too large images (over 256 KiB).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show my status only to buddies on my list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>When disabled, anyone can see your status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Block links from anonymous buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Protects you from potentially malicious links in messages from anonymous buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Gadu-Gadu Server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Custom server IP addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable file transfers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>External IP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Status Visibility</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You are going to reveal your status to several buddies which are currently not allowed to see it.
Are you sure to allow them to know you are available?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Make my status visible anyway</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stay with private status</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <source>Unknown Gender</source>
        <translation>Sexe inconnu</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Masculin</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Féminin</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Pseudo</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Sex</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Nom de famille</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Date de naissance</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Family city</source>
        <translation>Ville de la famille</translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <source>Gadu-Gadu number:</source>
        <translation>Numéro Gadu-Gadu :</translation>
    </message>
</context>
<context>
    <name>GaduProtocolPlugin</name>
    <message>
        <source>Gadu-Gadu Protocol</source>
        <translation>Protocol Gadu-Gadu</translation>
    </message>
    <message>
        <source>Cannot load Gadu-Gadu Protocol plugin. Please compile libgadu with zlib support.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <source>Remind password</source>
        <translation>Rappeler le mot de passe</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresse email</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Send Password</source>
        <translation>Envoyer le mot de passe</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error during remind password</source>
        <translation>Erreur durant le rappel de mot de passe</translation>
    </message>
    <message>
        <source>Your password has been sent on your email</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <source>Unregister account</source>
        <translation>Désinscrire le compte</translation>
    </message>
    <message>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Numéro Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unregister Account</source>
        <translation>Désinscrire le compte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulez</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Registering new Gadu-Gadu account</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>