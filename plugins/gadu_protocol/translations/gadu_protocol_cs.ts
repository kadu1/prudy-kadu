<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Unable to connect, server has not been found</source>
        <translation>Nepodařilo se navázat spojení, server nebyl nalezen</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation>Nepodařilo se navázat spojení</translation>
    </message>
    <message>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation>Změňte, prosím, svoji adresu elektronické pošty v okně &quot;Změnit heslo / E-mail&quot;. Pole pro nové heslo ponechejte prázdné.</translation>
    </message>
    <message>
        <source>Unable to connect, server has returned unknown data</source>
        <translation>Nepodařilo se navázat spojení, server vrátil neznámá data</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during reading</source>
        <translation>Nepodařilo se navázat spojení, přerušení spojení během čtení</translation>
    </message>
    <message>
        <source>Unable to connect, connection break during writing</source>
        <translation>Nepodařilo se navázat spojení, přerušení spojení během zápisu</translation>
    </message>
    <message>
        <source>Unable to connect, invalid password</source>
        <translation>Nepodařilo se navázat spojení, neplatné heslo</translation>
    </message>
    <message>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation>Nepodařilo se navázat spojení, chyba ve vyjednání TLS</translation>
    </message>
    <message>
        <source>Too many connection attempts with bad password!</source>
        <translation>Příliš mnoho pokusů o spojení se špatným heslem!</translation>
    </message>
    <message>
        <source>Unable to connect, servers are down</source>
        <translation>Nepodařilo se navázat spojení, servery spadly</translation>
    </message>
    <message>
        <source>Connection broken</source>
        <translation>Připojení přerušeno</translation>
    </message>
    <message>
        <source>Connection timeout!</source>
        <translation>Časové omezení pro spojení. Spojení vypršelo!</translation>
    </message>
    <message>
        <source>Disconnection has occurred</source>
        <translation>Vyskytlo se odpojení</translation>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Číslo Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Remember Password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Přidat účet</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Zapomněl jste své heslo?</translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <source>Change Password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresa elektronické pošty</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Napište adresu elektronické pošty použitou běhempřihlášení účtu (registrace).&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation>Staré heslo</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Napište současné heslo pro svůj účet Gadu-Gadu.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation>Nové heslo</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Napište nové heslo pro svůj účet Gadu-Gadu.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Retype New Password</source>
        <translation>Napište nové heslo znovu</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Znaky</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Za účelem ověření napište, prosím, znaky výše.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Changing password was successful.</source>
        <translation>Změna hesla byla úspěšná.</translation>
    </message>
    <message>
        <source>An error has occurred. Please try again later.</source>
        <translation>Vyskytla se chyba. Zkuste to, prosím, znovu později.</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Do požadovaných polí byla zapsána chybná data.

Hesla zapsaná v obou polích (&quot;Heslo&quot; a &quot;Napište heslo znovu&quot;) musí být stejná!</translation>
    </message>
</context>
<context>
    <name>GaduChatImageService</name>
    <message>
        <source>This image has %1 KiB and exceeds recommended maximum size of %2 KiB. Some clients may have trouble with too large images.</source>
        <translation>Tento obrázek má %1 KiB a překračuje doporučenou největší velikost %2 KiB. Někteří klienti mají potíže s příliš velkými obrázky.</translation>
    </message>
    <message>
        <source>Do you really want to send this image?</source>
        <translation>Opravdu chcete poslat tento obrázek?</translation>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Message too long (%1 &gt;= %2)</source>
        <translation>Zpráva příliš dlouhá (%1 &gt;= %2)</translation>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <source>First Name</source>
        <translation>Křestní jméno</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Pohlaví</translation>
    </message>
    <message>
        <source>Birthdate</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>State/Province</source>
        <translation>Stát/Země</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>Adresa IP</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <source>DNS Name</source>
        <translation>Název DNS</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>Verze protokolu</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Žena</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Muž</translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Retype Password</source>
        <translation>Napište heslo znovu</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresa elektronické pošty</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Znaky</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Za účelem ověření napište, prosím, znaky výše.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Regster Account</source>
        <translation>Přihlásit účet</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) must be the same!</source>
        <translation>Do požadovaných polí byla zapsána chybná data.

Hesla zapsaná v obou polích (&quot;Heslo&quot; a &quot;Napište heslo znovu&quot;) musí být stejná!</translation>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Smazat účet</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Číslo Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <source>Forgot Your Password?</source>
        <translation>Zapomněl jste své heslo?</translation>
    </message>
    <message>
        <source>Change Your Password</source>
        <translation>Změnit své heslo</translation>
    </message>
    <message>
        <source>Account Identity</source>
        <translation>Totožnost účtu</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Vyberte nebo zadejte totožnost, která bude spojena s tímto účtem.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Osobní údaje</translation>
    </message>
    <message>
        <source>Buddies</source>
        <translation>Kamarádi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Připojení</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <source>Use default servers</source>
        <translation>Použít výchozí servery</translation>
    </message>
    <message>
        <source>Use encrypted connection</source>
        <translation>Použít šifrované připojení</translation>
    </message>
    <message>
        <source>Remove account</source>
        <translation>Odstranit účet</translation>
    </message>
    <message>
        <source>Remove account and unregister from server</source>
        <translation>Odstranit účet a odhlásit se ze serveru</translation>
    </message>
    <message>
        <source>External port</source>
        <translation>Vnější přípojka</translation>
    </message>
    <message>
        <source>You have to compile libgadu with SSL support to be able to enable encrypted connection</source>
        <translation>Musíte libgadu sestavit s podporou pro SSL, abyste mohl povolit zašifrované spojení</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Jiné</translation>
    </message>
    <message>
        <source>Proxy configuration</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Obrázky</translation>
    </message>
    <message>
        <source>Receive images also when I am Invisible</source>
        <translation>Přijímat obrázky, i když jsem neviditelný</translation>
    </message>
    <message>
        <source>Warn me when the image being sent may be too large</source>
        <translation>Varovat, když je posílaný obrázek příliš velký</translation>
    </message>
    <message>
        <source>Some clients may have trouble with too large images (over 256 KiB).</source>
        <translation>Někteří klienti mají potíže s příliš velkými obrázky (nad 256 KiB).</translation>
    </message>
    <message>
        <source>Show my status only to buddies on my list</source>
        <translation>Ukazovat můj stav jen kamarádům v mém seznamu</translation>
    </message>
    <message>
        <source>When disabled, anyone can see your status.</source>
        <translation>Když je zakázáno, kdokoli může vidět váš stav.</translation>
    </message>
    <message>
        <source>Enable composing events</source>
        <translation>Povolit složené události</translation>
    </message>
    <message>
        <source>Your interlocutor will be notified when you are typing a message, before it is sent. And vice versa.</source>
        <translation>Účastník vašeho hovoru bude upozorněn, když budete psát zprávu, předtím než bude poslána. A naopak.</translation>
    </message>
    <message>
        <source>Block links from anonymous buddies</source>
        <translation>Blokovat odkazy od nepodepsaných kamarádů</translation>
    </message>
    <message>
        <source>Protects you from potentially malicious links in messages from anonymous buddies</source>
        <translation>Chrání vás před možnými škodlivými odkazy ve zprávách od nepodepsaných kamarádů</translation>
    </message>
    <message>
        <source>Gadu-Gadu Server</source>
        <translation>Server Gadu-Gadu</translation>
    </message>
    <message>
        <source>Custom server IP addresses</source>
        <translation>Vlastní serverové adresy IP</translation>
    </message>
    <message>
        <source>Enable file transfers</source>
        <translation>Povolit přenosy souborů</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <source>External IP</source>
        <translation>Vnější IP</translation>
    </message>
    <message>
        <source>Confrim Account Removal</source>
        <translation>Potvrdit odstranění účtu</translation>
    </message>
    <message>
        <source>Are you sure do you want to remove account %1 (%2)?</source>
        <translation>Opravdu chcete odstranit účet %1 (%2)?</translation>
    </message>
    <message>
        <source>Status Visibility</source>
        <translation>Viditelnost stavu</translation>
    </message>
    <message>
        <source>You are going to reveal your status to several buddies which are currently not allowed to see it.
Are you sure to allow them to know you are available?</source>
        <translation>Chystáte se odhalit svůj stav několika kamarádům, kterým není v současnosti dovoleno jej vidět.
Jste si jistý, že jim chcete umožnit vědět, že jste dostupný?</translation>
    </message>
    <message>
        <source>Make my status visible anyway</source>
        <translation>Učinit můj stav viditelný tak jako tak</translation>
    </message>
    <message>
        <source>Stay with private status</source>
        <translation>Zůstat u soukromého stavu</translation>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <source>Unknown Gender</source>
        <translation>Neznámé pohlaví</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Muž</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Žena</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Křestní jméno</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>Sex</source>
        <translation>Pohlaví</translation>
    </message>
    <message>
        <source>Family name</source>
        <translation>Rodné jméno</translation>
    </message>
    <message>
        <source>Birth year</source>
        <translation>Rok narození</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>Family city</source>
        <translation>Rodné město</translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <source>Gadu-Gadu number:</source>
        <translation>Číslo Gadu-Gadu:</translation>
    </message>
</context>
<context>
    <name>GaduProtocolPlugin</name>
    <message>
        <source>Gadu-Gadu Protocol</source>
        <translation>Protokol Gadu-Gadu</translation>
    </message>
    <message>
        <source>Cannot load Gadu-Gadu Protocol plugin. Please compile libgadu with zlib support.</source>
        <translation>Nelze nahrát přídavný modul pro protokol Gadu-Gadu. Sestavte, prosím, libgadu s podporou pro zlib.</translation>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <source>Remind password</source>
        <translation>Připomenout heslo</translation>
    </message>
    <message>
        <source>E-Mail Address</source>
        <translation>Adresa elektronické pošty</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Napište adresu elektronické pošty použitou během přihlášení účtu (registrace).&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Znaky</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Za účelem ověření napište, prosím, znaky výše.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Send Password</source>
        <translation>Poslat heslo</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Error during remind password</source>
        <translation>Chyba během připomínání hesla</translation>
    </message>
    <message>
        <source>Your password has been sent on your email</source>
        <translation>Vaše heslo bylo posláno na vaši e-mailovou adresu</translation>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <source>Unregister account</source>
        <translation>Odhlásit účet</translation>
    </message>
    <message>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation>Toto dialogové okénko vám umožní odhlášení vašeho účtu. Tuto funkci používejte opatrně.</translation>
    </message>
    <message>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Toto trvale smaže váš účet a vy jej nebudete moci použít později!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Gadu-Gadu number</source>
        <translation>Číslo Gadu-Gadu</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Napište heslo pro svůj účet Gadu-Gadu.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Znaky</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Za účelem ověření napište, prosím, znaky výše.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Unregister Account</source>
        <translation>Odhlásit účet</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation>Odhlášení proběhlo úspěšně. Nyní nemáte žádné číslo pro Gadu Gadu :(</translation>
    </message>
    <message>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation>Při odhlašování se vyskytla chyba. Zkuste to, prosím, znovu později.</translation>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation>Počkejte, prosím. Je zapisován nový účet Gadu-Gadu.</translation>
    </message>
    <message>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation>Přihlášení účtu proběhlo úspěšně. Vaše nové číslo je %1.
Uložte jej na bezpečném místě společně s heslem.
Nyní přidejte své přátele do seznamu uživatelů.</translation>
    </message>
    <message>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Při přihlašování se vyskytla chyba. Zkuste to, prosím, znovu později.</translation>
    </message>
    <message>
        <source>Registering new Gadu-Gadu account</source>
        <translation>Přihlašuje se nový účet Gadu-Gadu</translation>
    </message>
</context>
</TS>