<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Firewall</source>
        <translation>Firewall</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Notification syntax</source>
        <translation>Syntaxe de notification</translation>
    </message>
    <message>
        <source>%u - uin, %m - message</source>
        <translation>%u - uin, %m - message</translation>
    </message>
    <message>
        <source>Search for unknown person in directory and show window with his personal data.</source>
        <translation>Rechercher la personne inconnue dans le répertoire et afficher ses données personnelles.</translation>
    </message>
    <message>
        <source>Unknown chats protection</source>
        <translation>Protection des conversations d&apos;inconnu</translation>
    </message>
    <message>
        <source>Ignore conferences with all persons unknown</source>
        <translation>Ignorer les conférences avec toute personne inconnue</translation>
    </message>
    <message>
        <source>Protect against chats with unknown persons</source>
        <translation>Se protéger des conversations avec des personnes inconnues</translation>
    </message>
    <message>
        <source>Automatic question</source>
        <translation>Question automatique</translation>
    </message>
    <message>
        <source>Send confirmation</source>
        <translation>Envoyer une confirmation</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <source>Safe sending</source>
        <translation>Envoi protégé</translation>
    </message>
    <message>
        <source>Enable safe sending</source>
        <translation>Activer l&apos;envoi protégé</translation>
    </message>
    <message>
        <source>You must confirm each message sent to these people</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Message was firewalled</source>
        <translation>Le message a été bloqué par le firewall</translation>
    </message>
    <message>
        <source>Anonymous chat attempt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save anonymous chat attempt in history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save anonymous chat attempt in log file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Log file path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu will ask the question typed below, if someone wants talk to you and you do not have him on your list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Drop chats with unknown person when I am invisible/unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reaction on right answer</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Firewall</name>
    <message>
        <source>I want something</source>
        <translation>Je veux quelque chose</translation>
    </message>
    <message>
        <source>flooding DoS attack with emoticons!</source>
        <translation>Attaque flooding DoS avec émoticônes !</translation>
    </message>
    <message>
        <source>flooding DoS attack!</source>
        <translation>Attaque flooding DoS !</translation>
    </message>
    <message>
        <source>Chat with anonim silently dropped.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK, now say hello, and introduce yourself ;-)</source>
        <translation>OK, maintenant dîtes bonjour et présentez-vous :-)</translation>
    </message>
    <message>
        <source>User wrote right answer!
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>This message has been generated AUTOMATICALLY!

I&apos;m a busy person and I don&apos;t have time for stupid chats. Find another person to chat with. If you REALLY want something from me, simple type &quot;I want something&quot; (capital doesn&apos;t matter)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Are you sure you want to send this message?</source>
        <translation>Êtes-vous sûr de vouloir envoyer ce message ?</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Oui</translation>
    </message>
    <message>
        <source>Yes and allow until chat closed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <source>%u writes</source>
        <translation>%u écrits</translation>
    </message>
    <message>
        <source>This message has been generated AUTOMATICALLY!

I&apos;m a busy person and I don&apos;t have time for stupid chats with the persons hiding itself. If you want to talk with me change the status to Online or Busy first.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Chat with invisible anonim ignored.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>      DATA AND TIME      ::   ID      :: MESSAGE
</source>
        <translation>      DONNÉES ET TEMPS      ::   ID      :: MESSAGE
</translation>
    </message>
</context>
<context>
    <name>FirewallConfigurationUiHandler</name>
    <message>
        <source>Move to &apos;Secured&apos;</source>
        <translation>Déplacer vers &apos;Sécurisé&apos;</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Move to &apos;All&apos;</source>
        <translation>Déplacer vers &apos;Tout&apos;</translation>
    </message>
    <message>
        <source>Secured</source>
        <translation>Sécurisé</translation>
    </message>
    <message>
        <source>This message will be send to unknown person.</source>
        <translation>Ce message a été envoyé a une personne inconnu.</translation>
    </message>
    <message>
        <source>Right answer for question above - you can use regexp.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Answer:</source>
        <translation>Réponse :</translation>
    </message>
    <message>
        <source>Message:</source>
        <translation>Message :</translation>
    </message>
</context>
<context>
    <name>FirewallNotification</name>
    <message>
        <source>Message was blocked</source>
        <translation>Le message a été bloqué</translation>
    </message>
    <message>
        <source>%u writes</source>
        <translation>%u écrits</translation>
    </message>
</context>
</TS>