<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Firewall</source>
        <translation>Firewall</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Notification syntax</source>
        <translation>Notifiche sintassi</translation>
    </message>
    <message>
        <source>%u - uin, %m - message</source>
        <translation>%u - uin, %m - messaggio</translation>
    </message>
    <message>
        <source>Search for unknown person in directory and show window with his personal data.</source>
        <translation>Cerca persone sconosciute nella cartella e visualizza i suoi dati personali</translation>
    </message>
    <message>
        <source>Unknown chats protection</source>
        <translation>Protezione di chat sconosciuta</translation>
    </message>
    <message>
        <source>Ignore conferences with all persons unknown</source>
        <translation>Ignora conferenze con persone sconosciute</translation>
    </message>
    <message>
        <source>Protect against chats with unknown persons</source>
        <translation>Protezione contro le chat con persone sconosciute</translation>
    </message>
    <message>
        <source>Automatic question</source>
        <translation>Domanda automatica</translation>
    </message>
    <message>
        <source>Send confirmation</source>
        <translation>Invia conferma</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation>Conferma</translation>
    </message>
    <message>
        <source>Safe sending</source>
        <translation>Invio sicuro</translation>
    </message>
    <message>
        <source>Enable safe sending</source>
        <translation>Abilita invio sicuro</translation>
    </message>
    <message>
        <source>You must confirm each message sent to these people</source>
        <translation>Conferma ogni messaggio inviato a queste persone</translation>
    </message>
    <message>
        <source>Message was firewalled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Anonymous chat attempt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save anonymous chat attempt in history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save anonymous chat attempt in log file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Log file path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu will ask the question typed below, if someone wants talk to you and you do not have him on your list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Drop chats with unknown person when I am invisible/unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reaction on right answer</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Firewall</name>
    <message>
        <source>I want something</source>
        <translation>Io voglio qualcosa</translation>
    </message>
    <message>
        <source>flooding DoS attack with emoticons!</source>
        <translation>Attacco flooding DoS con emoticons</translation>
    </message>
    <message>
        <source>flooding DoS attack!</source>
        <translation>Attacco flooding DoS</translation>
    </message>
    <message>
        <source>Chat with anonim silently dropped.
</source>
        <translation>Chatta con persone anonime silenziosamente scartate
</translation>
    </message>
    <message>
        <source>OK, now say hello, and introduce yourself ;-)</source>
        <translation>Ok, ora dici ciao e parla di te stesso ;-)</translation>
    </message>
    <message>
        <source>User wrote right answer!
</source>
        <translation>L&apos;utente ha scritto una risposta sbagliata
</translation>
    </message>
    <message>
        <source>This message has been generated AUTOMATICALLY!

I&apos;m a busy person and I don&apos;t have time for stupid chats. Find another person to chat with. If you REALLY want something from me, simple type &quot;I want something&quot; (capital doesn&apos;t matter)</source>
        <translation>Questo messaggio è stato generato AUTOMATICAMENTE
Io sono una persona occupata e non ho tempo per le stupide chat. Cerca un&apos;altra persona che chatti con te. Se vuoi realmente qualcosa da me, scrivi semplicemente: &quot;Io voglio qualcosa&quot;</translation>
    </message>
    <message>
        <source>Are you sure you want to send this message?</source>
        <translation>Sei sicuro di voler mandare questo messaggio?</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Si</translation>
    </message>
    <message>
        <source>Yes and allow until chat closed</source>
        <translation>Si e consenti finché la chat è chiusa</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <source>%u writes</source>
        <translation>%u scrive</translation>
    </message>
    <message>
        <source>This message has been generated AUTOMATICALLY!

I&apos;m a busy person and I don&apos;t have time for stupid chats with the persons hiding itself. If you want to talk with me change the status to Online or Busy first.</source>
        <translation>Questo messaggio è stato generato AUTOMATICAMENTE
Io sono una persona occupata e non ho tempo per le stupide chat con persone che nascondono se stessi. Se vuoi parlare con me, cambia prima il tuo stato su In linea o Occupato</translation>
    </message>
    <message>
        <source>Chat with invisible anonim ignored.
</source>
        <translation>Chat con anonimi invisibile ignorate
</translation>
    </message>
    <message>
        <source>      DATA AND TIME      ::   ID      :: MESSAGE
</source>
        <translation>      DATA E ORA      ::   ID      :: MESSAGGIO</translation>
    </message>
</context>
<context>
    <name>FirewallConfigurationUiHandler</name>
    <message>
        <source>Move to &apos;Secured&apos;</source>
        <translation>Sposta su &apos;Sicuro&apos;</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tutto</translation>
    </message>
    <message>
        <source>Move to &apos;All&apos;</source>
        <translation>Sposta su &apos;Tutto&apos;</translation>
    </message>
    <message>
        <source>Secured</source>
        <translation>Sicuro</translation>
    </message>
    <message>
        <source>This message will be send to unknown person.</source>
        <translation>Questo messaggio sarà inviato ad una persona sconosciuta.</translation>
    </message>
    <message>
        <source>Right answer for question above - you can use regexp.</source>
        <translation>Risposta corretta alla domanda - puoi usare le espressioni regolari.</translation>
    </message>
    <message>
        <source>Answer:</source>
        <translation>Domanda:</translation>
    </message>
    <message>
        <source>Message:</source>
        <translation>Messaggio</translation>
    </message>
</context>
<context>
    <name>FirewallNotification</name>
    <message>
        <source>Message was blocked</source>
        <translation>Il messaggio è stato bloccato</translation>
    </message>
    <message>
        <source>%u writes</source>
        <translation>%u scrive</translation>
    </message>
</context>
</TS>