<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Zone de notification</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Enveloppe clignotante</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Enveloppe statique</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Enveloppe d&apos;animation</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Start minimized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurer</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Réduire</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Quitter Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Statuses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Descriptions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Silent mode</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>