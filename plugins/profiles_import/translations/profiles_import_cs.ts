<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Zavést vnější profil Kadu 0.6.5</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Vybrat cestu k profilu</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Vybrat cestu k profilu:</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Vybrat totožnost zavedeného účtu:</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Zavést historii</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Zavést</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Totožnost nevybrána&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Vybraný adresář neobsahuje soubor kadu.conf.xml&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Zavést vnější profil...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Tento adresář není adresářem s profilem Kadu.
Soubor kadu.conf.xml nebyl nalezen</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>Profil úspěšně zaveden!</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>Nelze zavést profil: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Současná verze Kadu nepodporuje uživatelské profily.&lt;br /&gt;Místo toho je v jednom běhu Kadu podporováno více účtů.&lt;/p&gt;&lt;p&gt;Vyberte, prosím, profily, které chcete zavést jako&lt;br /&gt;účet pro tento běh Kadu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Zavést</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Zavést historii</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Zavést vnější profil...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>Profil %1 úspěšně zaveden!</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation>Zavést profil...</translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation>Nelze zavést profil: %1: %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation>Nelze otevřít soubor s profilem [%1].</translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation>Účet již existuje.</translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation>Zavedený účet nemá žádné ID</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation>Zavést profily...</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Zavést vnější profil...</translation>
    </message>
</context>
</TS>