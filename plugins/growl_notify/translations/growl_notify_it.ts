<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - titolo ( ad esempio Nuovo messaggio) %&amp;m - testo di notifica ( ad esempio Messaggio da Jim), %&amp;d - dettagli ( ad esempio quotazione messaggio),
%&amp;i - icona di notifica</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>TItolo</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sintassi</translation>
    </message>
</context>
</TS>