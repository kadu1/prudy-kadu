<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozhovor</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation>Povolit šifrování po přijetí zašifrované zprávy</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>Nelze použít veřejný klíč: není platným klíčem RSA</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>Nelze použít veřejný klíč: neplatné kódování BASE64</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>Nelze použít veřejný klíč: neplatné osvědčení PKCS1</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>Nelze použít veřejný klíč: tento klíč neumožňuje zašifrování</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>Nelze zašifrovat: platný veřejný klíč není dostupný</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>Nelze zašifrovat: platný čtverzubcův (blowfish) klíč není dostupný</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>Nelze zašifrovat: neznámá chyba</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation>Simlite</translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation>Poslat můj veřejný klíč (Simlite)</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Klíče nelze poslat. Prověřte, zda je nahrán přídavný modul encryption_ng_simlite</translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation>Veřejný klíč neexistuje. Chcete vytvořit nový?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation>Chyba při vytváření klíče</translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation>Není dostupný žádný veřejný klíč</translation>
    </message>
</context>
</TS>