<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>Impossible d&apos;utiliser la clef publique : clef RSA invalide</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>Impossible d&apos;utiliser la clef publique : encodage BASE64 invalide</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>Impossible d&apos;utiliser la clef publique : certificat PKCS1 invalide</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>Impossible d&apos;utiliser la clef publique : cette clef n&apos;autorise pas le cryptage</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>Cryptage impossible : aucune clef publique disponible</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>Cryptage impossible : aucune clef blowfish disponible</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>Cryptage impossible : erreur inconnue</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Error generating key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No public key available</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>