<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>PC Speaker</source>
        <translation>PC Speaker</translation>
    </message>
</context>
<context>
    <name>PCSpeakerConfigurationWidget</name>
    <message>
        <source>Put the played sounds separate by space, _ for pause, eg. D2 C1# G0</source>
        <translation>Put the played sounds separate by space, _ for pause, eg. D2 C1# G0</translation>
    </message>
</context>
</TS>