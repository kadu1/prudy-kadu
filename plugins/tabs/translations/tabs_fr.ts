<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Tabs</source>
        <translation>Onglets</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <source>Move tab left</source>
        <translation>Déplacer l&apos;onglet à gauche</translation>
    </message>
    <message>
        <source>Move tab right</source>
        <translation>Déplacer l&apos;onglet à droite</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Aller à l&apos;onglet précédent</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Aller à l&apos;onglet suivant</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Enable tabs in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>With this option, each chat will be opened in a separate tab in single window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Closing window closes current tab only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>When enabled, closing chat window will cause closing only current tab, instead of all tabs and the window itself</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tab bar position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose where you want the tab bar to be placed in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Behaviour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reopen closed tab</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>New message received, close window anyway?</source>
        <translation>Nouveau message reçu, fermer le fenêtre tout de même ?</translation>
    </message>
    <message>
        <source>Recent Chats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open Chat with...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tabs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Close Tab</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Close window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabsManager</name>
    <message>
        <source>Chat in New Window</source>
        <translation>Discuter dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <source>Chat in New Tab</source>
        <translation>Discuter dans un nouvelle onglet</translation>
    </message>
    <message>
        <source>Attach Chat to Tabs</source>
        <translation>Attacher une conversation à des onglets</translation>
    </message>
    <message>
        <source>NEW MESSAGE(S)</source>
        <translation>NOUVEAU(X) MESSAGE(S)</translation>
    </message>
    <message>
        <source>Detach</source>
        <translation>Détacher</translation>
    </message>
    <message>
        <source>Detach all</source>
        <translation>Tout détacher</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Close all</source>
        <translation>Fermer tous</translation>
    </message>
    <message>
        <source>Conference [%1]</source>
        <translation>Conférence [%1]</translation>
    </message>
    <message>
        <source>Close other tabs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reopen closed tab</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>