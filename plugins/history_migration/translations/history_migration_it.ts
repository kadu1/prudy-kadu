<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.0">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>La cronologia è stata importata in un nuovo formato. Attendi finché questo processo non sia terminato</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Progresso chat:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Progresso messaggio:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Importa cronologia...</translation>
    </message>
</context>
</TS>