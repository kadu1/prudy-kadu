<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>History is now being imported into new format. Please wait until this task is finished.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Chats progress:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Messages progress:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Import history...</translation>
    </message>
</context>
</TS>