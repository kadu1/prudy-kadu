<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation>Icons</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation>Emoticons</translation>
    </message>
    <message>
        <source>Enable emoticons</source>
        <translation>Enable emoticons</translation>
    </message>
    <message>
        <source>Animate emoticons</source>
        <translation>Animate emoticons</translation>
    </message>
    <message>
        <source>Icon theme</source>
        <translation>Icon theme</translation>
    </message>
    <message>
        <source>Choose emoticons theme</source>
        <translation>Choose emoticons theme</translation>
    </message>
    <message>
        <source>Install new emoticons...</source>
        <translation>Install new emoticons...</translation>
    </message>
</context>
<context>
    <name>EmoticonsConfigurationUiHandler</name>
    <message>
        <source>Open icon theme archive</source>
        <translation>Open icon theme archive</translation>
    </message>
    <message>
        <source>XZ archive (*.tar.xz)</source>
        <translation>XZ archive (*.tar.xz)</translation>
    </message>
    <message>
        <source>Installation failed</source>
        <translation>Installation failed</translation>
    </message>
</context>
<context>
    <name>InsertEmoticonAction</name>
    <message>
        <source>Insert Emoticon</source>
        <translation>Insert Emoticon</translation>
    </message>
    <message>
        <source>Insert emoticon</source>
        <translation>Insert emoticon</translation>
    </message>
    <message>
        <source>Insert emoticon - enable in configuration</source>
        <translation>Insert emoticon - enable in configuration</translation>
    </message>
</context>
</TS>