<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Dymki</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Układ</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>Dodaj nowy dymek</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Automatycznie</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>Własna pozycja dymków</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Minimalna szerokość</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Maksymalna szerokość</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>Podgląd pozycji dymków</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Czas zaniku</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Kolor czcionki</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Kolor tła</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Czcionka</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Lewy</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Nic</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Środkowy</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Prawy</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>&apos;Otwórz rozmowę&apos; działa z każdym zdarzeniem</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Pokaż treść wiadomości w dymku</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Liczba cytowanych znaków</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Usuń oczekujące wiadomości przy usuwaniu dymka</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Lista znajomych</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Dymek nad listą znajomych</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Kolor ramki</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Szerokość ramki</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Przezroczystość</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Przyciski myszy</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Składnia</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Rozmiar ikon</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>Nowa rozmowa lub wiadomość</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Zaawansowane</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n sekunda</numerusform><numerusform>%n sekundy</numerusform><numerusform>%n sekund</numerusform></translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Margines</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>Róg dymka</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>Na górze</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>Na dole</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>Lewy górny</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>Prawy górny</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>Lewy dolny</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>Prawy dolny</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Otwórz rozmwę</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>Usuń dymek</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Usuń wszystkie dymki</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Tutaj&lt;/b&gt; możesz zobaczyć podgląd</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Pokazuj przyciski tylko wtedy, gdy jest wymagana akcja użytkownika</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>Pozycja i rozmiar dymków...</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;file:///@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Dymek nad listą znajomych</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Odśwież podgląd</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Składnia</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Konfiguruj</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Dymek nad listą znajomych: </translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Zaawansowana konfiguracja dymków</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Konfiguruj</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Tutaj&lt;/b&gt; możesz zobaczyć podgląd</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>Konfiguracja dymków</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Nie ukrywaj</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Tutaj&lt;/b&gt; możesz zobaczyć podgląd</translation>
    </message>
</context>
</TS>