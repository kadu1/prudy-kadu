<?xml version="1.0" ?><!DOCTYPE TS><TS language="en" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Check idle every</source>
        <translation>Check idle every</translation>
    </message>
    <message>
        <source>Set status to away after</source>
        <translation>Set status to away after</translation>
    </message>
    <message>
        <source>Set status to invisible after</source>
        <translation>Set status to invisible after</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>Replace</translation>
    </message>
    <message>
        <source>New description</source>
        <translation>New description</translation>
    </message>
    <message>
        <source>Set status to extended away after</source>
        <translation>Set status to extended away after</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation><numerusform>%n second</numerusform><numerusform>%n seconds</numerusform></translation>
    </message>
    <message>
        <source>Do Not Change</source>
        <translation>Do Not Change</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Behaviour</translation>
    </message>
    <message>
        <source>Inactivity</source>
        <translation>Inactivity</translation>
    </message>
    <message>
        <source>Refresh status every</source>
        <translation>Refresh status every</translation>
    </message>
    <message>
        <source>Switch to &apos;away&apos;</source>
        <translation>Switch to &apos;away&apos;</translation>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <translation><numerusform>%n minute(s)</numerusform><numerusform>%n minute(s)</numerusform></translation>
    </message>
    <message>
        <source>Switch to &apos;auto extended away&apos;</source>
        <translation>Switch to &apos;auto extended away&apos;</translation>
    </message>
    <message>
        <source>Switch to &apos;invisible&apos;</source>
        <translation>Switch to &apos;invisible&apos;</translation>
    </message>
    <message>
        <source>Switch to &apos;offline&apos;</source>
        <translation>Switch to &apos;offline&apos;</translation>
    </message>
    <message>
        <source>Set status to offline after</source>
        <translation>Set status to offline after</translation>
    </message>
    <message>
        <source>Status description when inactive</source>
        <translation>Status description when inactive</translation>
    </message>
    <message>
        <source>Prepend</source>
        <translation>Prepend</translation>
    </message>
    <message>
        <source>Append</source>
        <translation>Append</translation>
    </message>
</context>
<context>
    <name>AutoAway</name>
    <message>
        <source>Don&apos;t refresh</source>
        <translation>Don&apos;t refresh</translation>
    </message>
</context>
</TS>