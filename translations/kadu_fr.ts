<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>@default</name>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Yesterday at </source>
        <translation>Hier à</translation>
    </message>
    <message>
        <source>dddd at </source>
        <translation>dddd à</translation>
    </message>
    <message>
        <source>%1 weeks ago at </source>
        <translation>il y a %1 semaines de</translation>
    </message>
    <message>
        <source>%1 weeks and %2 days ago at </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 weeks and day ago at </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>week ago at </source>
        <translation>une semaine à</translation>
    </message>
    <message>
        <source>week and day ago at </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>week and %2 days ago at </source>
        <translation>il y une semaine et %2 jours de  </translation>
    </message>
    <message>
        <source>d MMMM yyyy at </source>
        <translation>d MMMM yyyy à </translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Chat header separators height</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Message separators height</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remove chat header repetitions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Interval between header removal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Hide server time</source>
        <translation>Masquer l&apos;heure du serveur</translation>
    </message>
    <message>
        <source>Maximum time difference</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Chat window title syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Conference window title prefix</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Conference window title syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>La langue</translation>
    </message>
    <message>
        <source>Choose your language</source>
        <translation>Choisissez votre langue</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation>Applications</translation>
    </message>
    <message>
        <source>Custom browser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Write specific path to your browser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Custom e-mail client</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Write specific path to your email client</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>Allow executing commands by parser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Debugging mask</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <source>Ignore messages from anonymous users</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore richtext from anonymous users</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Status</source>
        <translation>Le statut</translation>
    </message>
    <message>
        <source>On startup, set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose status which will be set on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Online</source>
        <translation>En ligne</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <source>Define description which will be set on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Define description which will be set on shutdown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Number of kept descriptions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Allow using variables in status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Icon theme</source>
        <translation>Thème d&apos;icône</translation>
    </message>
    <message>
        <source>Choose icon theme</source>
        <translation>Choisir un thème d&apos;icône</translation>
    </message>
    <message>
        <source>Show status changing button</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Colors</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Chat window</source>
        <translation>Fenêtre de conversation</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Fond</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Descriptions font</source>
        <translation>Police des descriptions</translation>
    </message>
    <message>
        <source>Information Panel</source>
        <translation>Panneau d&apos;information</translation>
    </message>
    <message>
        <source>Use custom background color</source>
        <translation>Utiliser une couleur de fond personnalisée</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation>Les fontes</translation>
    </message>
    <message>
        <source>Show number of new messages on chat title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Choose style of chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <source>Display group tabs</source>
        <translation>Afficher le groupe d&apos;onglets</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descriptions</translation>
    </message>
    <message>
        <source>Use background image</source>
        <translation>Utiliser une image de fond</translation>
    </message>
    <message>
        <source>Background file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tiled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Centered</source>
        <translation>Centré</translation>
    </message>
    <message>
        <source>Stretched</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose information panel style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <source>Manage modules</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open chat with...</source>
        <translation>Lancer une conversation avec ...</translation>
    </message>
    <message>
        <source>Clear chat</source>
        <translation>Effacer la conversation</translation>
    </message>
    <message>
        <source>Close chat</source>
        <translation>Fermer la conversation</translation>
    </message>
    <message>
        <source>Bold text</source>
        <translation>le texte en gras</translation>
    </message>
    <message>
        <source>Italic text</source>
        <translation>Texte en italique</translation>
    </message>
    <message>
        <source>Underline text</source>
        <translation>texte souligné</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Comportement</translation>
    </message>
    <message>
        <source>Remember chat windows positions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open chat window on new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open chat window when a new message arrives</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open chat window on new message only when online</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear recent chats list on exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Block chat window when a new message arrives</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <source>default</source>
        <translation>par défaut</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>Set invisible if last status was &apos;Offline&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Displays messages from anonymous users without formatting (colors, images, font weight...).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection error</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <source>New chat</source>
        <translation>Nouveau chat</translation>
    </message>
    <message>
        <source>New message</source>
        <translation>Nouveau message</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Silent Mode</source>
        <translation>Mode silencieux</translation>
    </message>
    <message>
        <source>Disable notifications when my status is set to &apos;Do not Disturb&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Disable notifications when a fullscreen application is active</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Disables notifications when running a fullscreen application: movie player, game, etc.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Notify about new messages only when window is inactive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore changes right after connection to the server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Multilogon</source>
        <translation>Identification multiple</translation>
    </message>
    <message>
        <source>Multilogon session connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Multilogon session disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>User changed status</source>
        <translation>L&apos;utilisateur a changé d&apos;état</translation>
    </message>
    <message>
        <source>to free for chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to online</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to away</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to do not disturb</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to offline</source>
        <translation>Ouvrez la conversation avec ...</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation>Bloqué</translation>
    </message>
    <message>
        <source>Blocking</source>
        <translation>Blocage</translation>
    </message>
    <message>
        <source>Headers and Separators</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Server Time</source>
        <translation>Heure du serveur</translation>
    </message>
    <message>
        <source>Window Title</source>
        <translation>Titre de la fenêtre</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Enter a nickname for your account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu needs to be restarted before changes to the language settings will take effect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Web and E-Mail</source>
        <translation>Web et email</translation>
    </message>
    <message>
        <source>Kadu will use default applications to open links from chat messages and buddy descriptions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use System Web Browser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use System E-Mail Client</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Check for updates when Kadu is opened</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Automatically open Kadu at startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show offline buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddies that are ignored</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddies which are blocking me</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Chat Window</source>
        <translation>Fenêtre de conversation</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Buddy list</source>
        <translation>Liste d&apos;amis</translation>
    </message>
    <message>
        <source>Displays group tabs on the buddy list</source>
        <translation>Afficher le groupe d&apos;onglets dans la liste des amis</translation>
    </message>
    <message>
        <source>Show connected buddies in bold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy Photos</source>
        <translation>Photos des amis</translation>
    </message>
    <message>
        <source>Grey out offline buddies&apos; photos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddies who are offline will have their photos greyed out</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Displays descriptions in the buddy list</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Dislpays multiline descriptions in the buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Define background file for the buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose buddy list background style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add buddy</source>
        <translation>Ajouter un ami</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Remove from buddy list</source>
        <translation>Supprimer de la liste d&apos;amis</translation>
    </message>
    <message>
        <source>Search this buddy in directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show / hide offline buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show / hide buddies without description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Status Change</source>
        <translation>Modifier l&apos;état</translation>
    </message>
    <message>
        <source>This option will supersede tooltips with buddies' status
changes upon establishing connection to the server</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>File transfer</source>
        <translation>Transfert de fichiers</translation>
    </message>
    <message>
        <source>Incoming file transfer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Style variant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Not available</source>
        <translation>Indisponible</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <source>Send message</source>
        <translation>Envoyer le message</translation>
    </message>
    <message>
        <source>Show a window with notification</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact state change notification position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Beginning of title</source>
        <translation>Début du titre</translation>
    </message>
    <message>
        <source>End of title</source>
        <translation>Fin du titre</translation>
    </message>
    <message>
        <source>Custom composing notification syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Move the Window to the Active Desktop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Switch Desktop Making the Window Visible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Set status</source>
        <translation>Sélectionner l&apos;état</translation>
    </message>
    <message>
        <source>For All Accounts</source>
        <translation>Pour tous les comptes</translation>
    </message>
    <message>
        <source>Last Status</source>
        <translation>Dernier état</translation>
    </message>
    <message>
        <source>Free for Chat</source>
        <translation>Disponible pour discuter</translation>
    </message>
    <message>
        <source>Do Not Disturb</source>
        <translation>Ne pas déranger</translation>
    </message>
    <message>
        <source>My nick color</source>
        <translation>Ma couleur de pseudo</translation>
    </message>
    <message>
        <source>Buddy&apos;s nick color</source>
        <translation>Couleur de pseudo des amis</translation>
    </message>
    <message>
        <source>My messages&apos; background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy&apos;s messages&apos; background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom chat background color</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Chat background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom text edit colors</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Text edit background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Text edit font</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Information panel</source>
        <translation>Panneau d&apos;information</translation>
    </message>
    <message>
        <source>Tiled and Centered</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open chat window only when I am online</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Syntax: %s - status, %d - description, %i - ip, %n - nick, %a - altnick, %f - first name
%r - surname, %m - mobile, %u - uin, %g - group
%h - gg version, %v - revDNS, %p - port, %e - email, %x - max image size, %z - gender (0/1/2)
</source>
        <translation>Syntaxe : %s - état, %d - description, %i - ip, %n - pseudo, %a - autrepseudo, %f - prénom
%r - surnom, %m - portable, %u - uin, %g - groupe
%h - gg version, %v - revDNS, %p - port, %e - email, %x - taille image max, %z - sexe (0/1/2)
</translation>
    </message>
    <message>
        <source>Syntax: %s - status, %d - description, %i - ip, %n - nick, %a - altnick, %f - first name
%r - surname, %m - mobile, %u - uin, %g - group
%h - gg version, %v - revDNS, %p - port, %e - email, %x - max image size, %z - gender (0/1/2),
#{protocol} - protocol that triggered event,
#{event} - name of event,
</source>
        <translation>Syntaxe : %s - état, %d - description, %i - ip, %n - pseudo, %a - autrepseudo, %f - prénom
%r - surnom, %m - portable, %u - uin, %g - groupe
%h - gg version, %v - revDNS, %p - port, %e - email, %x - taille image max, %z - sexe (0/1/2),
#{protocol} - protocole qui déclenche les événements,
#{event} - nom de l&apos;événement,
</translation>
    </message>
    <message>
        <source>Personal</source>
        <translation>Personnel</translation>
    </message>
    <message>
        <source>Default proxy</source>
        <translation>Proxy par défaut</translation>
    </message>
    <message>
        <source>Hide main window icon from taskbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Select window activation method</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Turn on blur</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Background colors</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>We&apos;re sorry, but Kadu cannot be loaded. Profile is inaccessible. Please check permissions in the &apos;%1&apos; directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Profile Inaccessible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Launch Kadu at startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Inform about new versions of Kadu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu main window will not be visible in window list on your taskbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Parser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Misc</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Install new icons...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable transparency in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your system does not support transparency.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddies list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddies&apos; photos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add border to buddies&apos; photos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddies&apos; descriptions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Split long descriptions into multiple lines</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Transparency</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable transparency in buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Transparency level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>My message font color</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy&apos;s message font color</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom font in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use custom font even for chat styles which define their own fonts (most Adium styles do that)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Limit visible messages in chat window to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Maximum number of messages visible in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fold links in messages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shorten ugly, long links to save space in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Use descriptive date format</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display dates like Today, Yesterday or Friday, 2:30PM</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Chat syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Configure...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddies window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddy information panel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Displays buddy information panel below the buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Displays status changing buttons below the buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy groups</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Tooltip over buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show buddy expanding button</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show an icon at each buddy that allows viewing contacts attached to this buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Displays buddies that are online using a bold font</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy icon position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose buddy icon alignment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>At the buddy&apos;s name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy list information style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose how buddy information is presented when cursor is hovering over buddy list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Notify about chat events in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Notify about chat events in windows&apos; titles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>New Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Block window close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Flash chat title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore status changes available - busy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remember opened chats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save opened chat windows when exiting and restore them on next statup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enable sending message by hitting &apos;Enter&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Hitting &apos;Enter&apos; while typing in chat window sends message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ask before clearing messages in chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ask for confirmation before clearing chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Per account separately</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Per identity separately</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose description which will be set on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Last Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>New Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>On shutdown, set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose description which will be set on shutdown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Current Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>New description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Store descriptions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Every description you set will be saved for further use.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Extras</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show tab Everybody</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Always show tab Ungroupped</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation>A propos de</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation>A&amp;uteurs</translation>
    </message>
    <message>
        <source>&amp;Thanks</source>
        <translation>&amp;Remerciements</translation>
    </message>
    <message>
        <source>&amp;ChangeLog</source>
        <translation>&amp;ChangeLog</translation>
    </message>
    <message>
        <source>&amp;License</source>
        <translation>L&amp;icence</translation>
    </message>
    <message>
        <source>Instant Messenger</source>
        <translation>Messagerie Instantanée</translation>
    </message>
    <message>
        <source>Support:</source>
        <translation>Support :</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <source>Qt %2 (compiled with Qt %3)</source>
        <translation>Qt %2 (compilé avec Qt %3)</translation>
    </message>
</context>
<context>
    <name>AccountAvatarWidget</name>
    <message>
        <source>Remove Photo...</source>
        <translation>Supprimer la photo...</translation>
    </message>
    <message>
        <source>Change Photo...</source>
        <translation>Modifier la photo...</translation>
    </message>
    <message>
        <source>Select avatar file</source>
        <translation>Sélectionner un avatar</translation>
    </message>
    <message>
        <source>Images (*.jpeg *.jpg *.png);;All Files (*)</source>
        <translation>Images (*.jpeg *.jpg *.png);; Tous les fichiers (*)</translation>
    </message>
</context>
<context>
    <name>AccountBuddyListWidget</name>
    <message>
        <source>Restore from file</source>
        <translation>Supprimer du fichier</translation>
    </message>
    <message>
        <source>Store to file</source>
        <translation>Stocker dans un fichier</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <source>Contacts list couldn&apos;t be imported. File %0 doesn&apos;t contain correct contacts list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact List Files (*.txt *.xml);;All Files (*)</source>
        <translation>Fichiers Liste de Contacts (*.txt *.xml);; Tous les fichiers (*)</translation>
    </message>
    <message>
        <source>Contact List Files (*.txt)</source>
        <translation>Fichiers Liste de Contacts (*.txt)</translation>
    </message>
</context>
<context>
    <name>AccountEventListener</name>
    <message>
        <source>Multilogon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Multilogon session connected from %1 at %2 with %3 for %4 account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Multilogon session disconnected from %1 at %2 with %3 for %4 account</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AccountManager</name>
    <message>
        <source>Please provide password for %1 (%2) account</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AccountsComboBox</name>
    <message>
        <source> - Select account - </source>
        <translation> - Sélectionner un compte - </translation>
    </message>
</context>
<context>
    <name>AddBuddyWindow</name>
    <message>
        <source>Add in group:</source>
        <translation>Ajouter un groupe dans :</translation>
    </message>
    <message>
        <source>Visible name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enter a name for this buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Merge with existing buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ask contact for authorization</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Allow buddy to see me when I&apos;m available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add buddy</source>
        <translation>Ajouter un ami</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <source>E-mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>User ID:</source>
        <translation>Identifiant de l&apos;utilisateur :</translation>
    </message>
    <message>
        <source>Mobile number:</source>
        <translation>Numéro de portable :</translation>
    </message>
    <message>
        <source>E-mail address:</source>
        <translation>Adresse email :</translation>
    </message>
    <message>
        <source>Account is not selected</source>
        <translation>Le compte n&apos;est pas sélectionné</translation>
    </message>
    <message>
        <source>You must be connected to add contacts to this account</source>
        <translation>vous devez être connecté pour ajouter des contacts à ce compte</translation>
    </message>
    <message>
        <source>This contact is already available as &lt;i&gt;%1&lt;/i&gt;</source>
        <translation>Ce contact est déjà disponible en tant que &lt;i&gt;%1&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Select buddy to merge with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Visible name is already used for another buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Entered mobile number is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Entered e-mail is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Entered user identification is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No user identification entered</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No mobile number entered</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No e-mail entered</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>Merge with:</source>
        <translation>Fusionner avec :</translation>
    </message>
    <message>
        <source>Enter visible name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Merge with buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source> - Select buddy - </source>
        <translation> - Sélectionner un ami - </translation>
    </message>
</context>
<context>
    <name>AddConferenceAction</name>
    <message>
        <source>Add Conference...</source>
        <translation>Ajouter une conférence...</translation>
    </message>
</context>
<context>
    <name>AddConferenceWindow</name>
    <message>
        <source>Add Conference</source>
        <translation>Ajouter une conférence</translation>
    </message>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>Visible name:</source>
        <translation>Nom visible :</translation>
    </message>
    <message>
        <source>Enter a name for this conference if you want to have it on roster</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Start Conference</source>
        <translation>Démarrer une conférence</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Account is not selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Select at least two buddies</source>
        <translation>Sélectionner au mois deux amis</translation>
    </message>
    <message>
        <source>Visible name is already used for another chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>This conference is already available as &lt;i&gt;%1&lt;/i&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enter visible name to add this conference to roster</source>
        <translation>Saisir un nom visible pour ajouter cette conférence à roster</translation>
    </message>
</context>
<context>
    <name>AddGroupDialogWidget</name>
    <message>
        <source>Add Group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AddRoomChatAction</name>
    <message>
        <source>Join Room...</source>
        <translation>Rejoindre un canal...</translation>
    </message>
</context>
<context>
    <name>AddRoomChatWindow</name>
    <message>
        <source>Join Room</source>
        <translation>Rejoindre un canal...</translation>
    </message>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>Visible name:</source>
        <translation>Nom visible :</translation>
    </message>
    <message>
        <source>Enter a name for this conference if you want to have it on roster</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Room:</source>
        <translation>Canal :</translation>
    </message>
    <message>
        <source>Nick:</source>
        <translation>Pseudo :</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <source>Add Room Chat</source>
        <translation>Ajouter un canal de conversation</translation>
    </message>
    <message>
        <source>Start Room Chat</source>
        <translation>Démarrer un canal de conversation</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Account is not selected</source>
        <translation>Le compte n&apos;est pas sélectionné</translation>
    </message>
    <message>
        <source>Enter room name</source>
        <translation>Saisir en nom de canal</translation>
    </message>
    <message>
        <source>Enter nick</source>
        <translation>Saisir un pseudo</translation>
    </message>
    <message>
        <source>Visible name is already used for another chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>This room chat is already available as &lt;i&gt;%1&lt;/i&gt;</source>
        <translation>Ce canal de conversation est déjà disponible en tant que &lt;i&gt;%1&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Enter visible name to add this room chat to roster</source>
        <translation>Saisir le nom visible pour ajouter ce canal de conversation à roster</translation>
    </message>
</context>
<context>
    <name>AdiumChatStyleEngine</name>
    <message>
        <source>Conference [%1]</source>
        <translation>Conférence [%1]</translation>
    </message>
</context>
<context>
    <name>AdiumTimeFormatter</name>
    <message>
        <source>1st quarter</source>
        <translation>1er trimestre</translation>
    </message>
    <message>
        <source>2nd quarter</source>
        <translation>2ème trimestre</translation>
    </message>
    <message>
        <source>3rd quarter</source>
        <translation>3ème trimestre</translation>
    </message>
    <message>
        <source>4th quarter</source>
        <translation>4ème trimestre</translation>
    </message>
</context>
<context>
    <name>Buddy</name>
    <message>
        <source>Example description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Example identity</source>
        <translation>Exemple d&apos;identité</translation>
    </message>
</context>
<context>
    <name>BuddyAvatarWidget</name>
    <message>
        <source>Remove Custom Photo...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Change Photo...</source>
        <translation>Modifier la photo...</translation>
    </message>
    <message>
        <source>Select new photo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Images (*.png *.jpg *.bmp);;All Files (*)</source>
        <translation>Images (*.png *.jpg *.bmp);; Tous les fichiers (*)</translation>
    </message>
</context>
<context>
    <name>BuddyContactsTable</name>
    <message>
        <source>Move up</source>
        <translation>Aller vers le haut</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Aller vers le bas</translation>
    </message>
    <message>
        <source>Add contact</source>
        <translation>Ajouter un contact</translation>
    </message>
    <message>
        <source>Detach contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remove contact</source>
        <translation>Supprimer un contact</translation>
    </message>
    <message>
        <source>New buddy display name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Give name for new buddy for this contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Are you sure do you want to delete this contact from buddy &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation>Êtes-vous certain de vouloir supprimer ce contact des amis &lt;b&gt;%1&lt;/b&gt; ?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BuddyContactsTableModel</name>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Account</source>
        <translation>Compte</translation>
    </message>
    <message>
        <source>Synchronize</source>
        <translation>Synchroniser</translation>
    </message>
</context>
<context>
    <name>BuddyDataWindow</name>
    <message>
        <source>Buddy Properties - %1</source>
        <translation>Propriétés de l&apos;ami - %1</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <source>Personal Information</source>
        <translation>Informations personnelles</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>BuddyDeleteWindow</name>
    <message>
        <source>The following buddies will be deleted:&lt;br/&gt;%1.&lt;br/&gt;Are you sure?</source>
        <translation>Les amis suivants seront supprimés : &lt;br/&gt;%1.&lt;br/&gt;Êtes-vous certain ?</translation>
    </message>
    <message>
        <source>Please select additional data that will be removed:</source>
        <translation>Veuillez sélectionné les données supplémentaires qui seront supprimées :</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>BuddyGeneralConfigurationWidget</name>
    <message>
        <source>Visible Name</source>
        <translation>Nom visible</translation>
    </message>
    <message>
        <source>Buddy contacts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Communication Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <source>Prefer the most available contact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&lt;p&gt;When enabled and one of this buddy&apos;s contacts has higher status (i.e., more available) than the others, that contact will be considered preferred regardless of its priority&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Protocol used by this buddy&apos;s contact does not allow changing buddy&apos;s name client-side</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BuddyGroupsConfigurationWidget</name>
    <message>
        <source>Add &lt;b&gt;%1&lt;/b&gt; to the groups below by checking the box next to the appropriate groups.</source>
        <translation>Ajouter &lt;b&gt;%1&lt;/b&gt; aux groupes ci-dessous en cochant en face des groupes appropriés.</translation>
    </message>
</context>
<context>
    <name>BuddyOptionsConfigurationWidget</name>
    <message>
        <source>Allow buddy to see when I&apos;m available</source>
        <translation>Autoriser les amis à voir lorsque je suis disponible</translation>
    </message>
    <message>
        <source>Block buddy</source>
        <translation>Bloquer un ami</translation>
    </message>
    <message>
        <source>Notify when buddy&apos;s status changes</source>
        <translation>Notifier lorsque l&apos;état des amis change</translation>
    </message>
    <message>
        <source>Hide description</source>
        <translation>Masquer la description</translation>
    </message>
</context>
<context>
    <name>BuddyPersonalInfoConfigurationWidget</name>
    <message>
        <source>Buddy contact</source>
        <translation>Contact ami</translation>
    </message>
</context>
<context>
    <name>ChangeStatusAction</name>
    <message>
        <source>Change Status</source>
        <translation>Modifier l&apos;état</translation>
    </message>
</context>
<context>
    <name>ChatDataWindow</name>
    <message>
        <source>Chat Properties - %1</source>
        <translation>Propriétés de la conversation - %1</translation>
    </message>
    <message>
        <source>Visible Name</source>
        <translation>Nom visible</translation>
    </message>
    <message>
        <source>Add this chat to the groups below by checking the box next to the appropriate groups.</source>
        <translation>Ajouter cette conversation aux groupes ci-dessous en cochant en face des groupes appropriés.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Conversation</translation>
    </message>
</context>
<context>
    <name>ChatEditBox</name>
    <message>
        <source>Insert image</source>
        <translation>Insérer une image</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>This file is not readable</source>
        <translation>Ce fichier n&apos;est par lisible</translation>
    </message>
    <message>
        <source>Do you really want to send this image?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>This image has %1 KiB and may be too big for %2.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 appears to be offline and may not receive images.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>This image has %1 KiB and may be too big for %2 of %3 contacts in this conference.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 of %2 contacts appear to be offline and may not receive images.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Images (*.png *.PNG *.jpg *.JPG *.jpeg *.JPEG *.gif *.GIF *.bmp *.BMP);;All Files (*)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Send anyway</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ChatImageRequestService</name>
    <message>
        <source>Buddy %1 is attempting to send you an image of %2 KiB in size.
This exceeds your configured limits.
Do you want to accept this image anyway?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Accept image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Deny</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Incoming Image</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ChatNotification</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
</context>
<context>
    <name>ChatRoomEditWidget</name>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>Room:</source>
        <translation>Canal :</translation>
    </message>
    <message>
        <source>Nick:</source>
        <translation>Pseudo :</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
</context>
<context>
    <name>ChatStylesManager</name>
    <message>
        <source>Your message</source>
        <translation>Votre message</translation>
    </message>
    <message>
        <source>Message from Your friend</source>
        <translation>Message de votre ami</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <source>Conference with </source>
        <translation>Conférence avec </translation>
    </message>
    <message>
        <source>Chat with </source>
        <translation>Conversation avec</translation>
    </message>
    <message>
        <source>Chat window will be cleared. Continue?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot send message while being offline.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>%1 ended the conversation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>(Composing...)</source>
        <translation>(écrit...)</translation>
    </message>
    <message>
        <source>(Inactive)</source>
        <translation>(inactif)</translation>
    </message>
    <message>
        <source>Clear chat window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ChatWidgetActions</name>
    <message>
        <source>More...</source>
        <translation>Plus...</translation>
    </message>
    <message>
        <source>Return Sends Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear Messages in Chat Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Insert Image</source>
        <translation>Insérer une image</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&amp;Send</source>
        <translation>&amp;Envoyer</translation>
    </message>
    <message>
        <source>Block Buddy</source>
        <translation>Bloquer un ami</translation>
    </message>
    <message>
        <source>&amp;Chat</source>
        <translation>&amp;Conversation</translation>
    </message>
    <message>
        <source>Open Chat with...</source>
        <translation>Lancer une conversation avec ...</translation>
    </message>
    <message>
        <source>More</source>
        <translation>Plus</translation>
    </message>
</context>
<context>
    <name>ChatWindow</name>
    <message>
        <source>New message received, close window anyway?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Close window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ChooseIdentityWidget</name>
    <message>
        <source>Create new description...</source>
        <translation>Créer une nouvelle description...</translation>
    </message>
</context>
<context>
    <name>ConfigurationWindow</name>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>ConnectionErrorNotification</name>
    <message>
        <source>Connection error</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <source>Connection error on account: %1 (%2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ContactListService</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>The following contacts from your list were not found in file:&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;br/&gt;Do you want to remove them from contact list?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu since version 0.10.0 automatically synchronizes Gadu-Gadu contact list with server. Now the first synchronization will be performed.&lt;br/&gt;&lt;br/&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The following contacts present on the server were not found on your local contact list:&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;br/&gt;If you do not agree to add those contacts to your local list, they will be removed from the server.&lt;br/&gt;&lt;br/&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The following contacts from your local list are present on the server under different names:&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;br/&gt;&lt;br/&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Moreover, the following contacts from your local list are present on the server under different names:&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;br/&gt;&lt;br/&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Do you want to apply the above changes to your local contact list? Regardless of your choice, it will be sent to the server after making possible changes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Apply changes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Leave contact list unchanged</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>Me</source>
        <translation>Moi</translation>
    </message>
</context>
<context>
    <name>CustomInput</name>
    <message>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Tout sélectionner</translation>
    </message>
</context>
<context>
    <name>DefaultProxyAction</name>
    <message>
        <source>Select Default Proxy</source>
        <translation>Sélectionner le proxy par défaut</translation>
    </message>
    <message>
        <source> - No proxy - </source>
        <translation> - Aucun proxy - </translation>
    </message>
    <message>
        <source>Edit proxy configuration...</source>
        <translation>Modifier la configuration du proxy...</translation>
    </message>
</context>
<context>
    <name>DeleteTalkableAction</name>
    <message>
        <source>Delete Chat</source>
        <translation>Supprimer une conversation</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; chat will be deleted.&lt;br/&gt;Are you sure?</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; la conversation sera supprimée.&lt;br/&gt;Êtes-vous certain ?</translation>
    </message>
    <message>
        <source>Delete Buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EditTalkableAction</name>
    <message>
        <source>View Buddy Properties</source>
        <translation>Voir les propriétés de l&apos;ami</translation>
    </message>
</context>
<context>
    <name>FileTransferActions</name>
    <message>
        <source>Send File...</source>
        <translation>Envoyer un fichier...</translation>
    </message>
    <message>
        <source>View File Transfers</source>
        <translation>Voir les fichiers transférés</translation>
    </message>
    <message>
        <source>Select file location</source>
        <translation>Sélectionner un emplacement de fichier</translation>
    </message>
</context>
<context>
    <name>FileTransferManager</name>
    <message>
        <source>Select file location</source>
        <translation>Sélectionner un emplacement de fichier</translation>
    </message>
    <message>
        <source>File %1 already exists.</source>
        <translation>Le fichier %1 existe déjà.</translation>
    </message>
    <message>
        <source>Save file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <source>Resume</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <source>Select another file</source>
        <translation>Sélectionner un autre fichier</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Could not open file. Select another one.</source>
        <translation>Impossible d&apos;ouvrir le fichier. Sélectionnez en un autre.</translation>
    </message>
</context>
<context>
    <name>FileTransferWidget</name>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>File &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt; to &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;on account &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Fichier &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt; vers &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;le compte &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <source>File &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt; from &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;on account &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>Fichier &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt; depuis &lt;b&gt;%2&lt;/b&gt;&lt;br /&gt;le compte &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Are you sure you want to remove this transfer?</source>
        <translation>Êtes-vous certain de vouloir supprimer ce transfert ?</translation>
    </message>
    <message>
        <source>&lt;b&gt;Not connected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Non connecté&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Error&lt;/b&gt;</source>
        <translation>&lt;b&gt;Erreur&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Wait for connection&lt;/b&gt;</source>
        <translation>&lt;b&gt;En attente de connexion&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Wait for accept&lt;/b&gt;</source>
        <translation>&lt;b&gt;En attente d&apos;acceptation&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Transfer&lt;/b&gt;: %1 kB/s</source>
        <translation>&lt;b&gt;Transfert&lt;/b&gt; : %1 ko/s</translation>
    </message>
    <message>
        <source>&lt;b&gt;Finished&lt;/b&gt;</source>
        <translation>&lt;b&gt;Terminé&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Rejected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Refusé&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTransferWindow</name>
    <message>
        <source>Kadu - file transfers</source>
        <translation>Kadu - transfert de fichiers</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>FilterWidget</name>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>GroupFilterTabData</name>
    <message>
        <source>Ungrouped</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Everybody</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GroupManager</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is prohibited</source>
        <translation>&apos;%1&apos; est interdit</translation>
    </message>
    <message>
        <source>Numbers are prohibited</source>
        <translation>Les nombres sont interdits</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Group name %1 is prohibited</source>
        <translation>Le nom de groupe %1 est interdit</translation>
    </message>
    <message>
        <source>Group of that name already exists!</source>
        <translation>Un groupe de ce nom existe déjà !</translation>
    </message>
</context>
<context>
    <name>GroupPropertiesWindow</name>
    <message>
        <source>Properties of group %1</source>
        <translation>Propriétés du groupe %1</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation>Comportement</translation>
    </message>
    <message>
        <source>Notify about status changes</source>
        <translation>Notifier les changements d&apos;état</translation>
    </message>
    <message>
        <source>Offline for this group</source>
        <translation>hors ligne pour ce groupe</translation>
    </message>
    <message>
        <source>Supported for Gadu-Gadu network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Work only when network supports it</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show in group &quot;All&quot;</source>
        <translation>Afficher dans le groupe &quot;Tout&quot;</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>Use custom icon</source>
        <translation>Icône utilisateur personnalisée</translation>
    </message>
    <message>
        <source> Set Icon</source>
        <translation> Jeu d&apos;icônes</translation>
    </message>
    <message>
        <source>Show group name</source>
        <translation>Afficher le nom du groupe</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Choose an icon</source>
        <translation>Choisir une icône</translation>
    </message>
    <message>
        <source>Images (*.png *.xpm *.jpg);;All Files (*)</source>
        <translation>Images (*.png *.xpm *.jpg);; Tous les fichiers (*)</translation>
    </message>
</context>
<context>
    <name>GroupTabBar</name>
    <message>
        <source>Add Buddy</source>
        <translation>Ajouter un ami</translation>
    </message>
    <message>
        <source>Rename Group</source>
        <translation>Renommer le groupe</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation>Supprimer le groupe</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>New Group</source>
        <translation>Nouveau groupe</translation>
    </message>
    <message>
        <source>Please enter the name for the new group:</source>
        <translation>Veuillez saisir le nom du nouveau groupe :</translation>
    </message>
    <message>
        <source>Move to group %1</source>
        <translation>Déplacer dans le groupe %1</translation>
    </message>
    <message>
        <source>Add to group %1</source>
        <translation>Ajouter au groupe %1</translation>
    </message>
    <message>
        <source>Add Group</source>
        <translation>Ajouter un groupe</translation>
    </message>
    <message>
        <source>Please enter a new name for the &lt;i&gt;%0&lt;/i&gt; group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Edit Group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Group &lt;i&gt;%0&lt;/i&gt; will be deleted, but without buddies. Are you sure?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please enter the name for the new group</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GroupsComboBox</name>
    <message>
        <source>Create a new group...</source>
        <translation>Créer un nouveau groupe...</translation>
    </message>
    <message>
        <source>New Group</source>
        <translation>Nouveau groupe</translation>
    </message>
    <message>
        <source>Please enter the name for the new group:</source>
        <translation>Veuillez saisir le nom du nouveau groupe :</translation>
    </message>
    <message>
        <source> - Do not add - </source>
        <translation> - Ne pas ajouter - </translation>
    </message>
</context>
<context>
    <name>HtmlMessagesRenderer</name>
    <message>
        <source>%1 is active</source>
        <translation>%1 est actif</translation>
    </message>
    <message>
        <source>%1 is composing...</source>
        <translation>%1 écrit</translation>
    </message>
    <message>
        <source>%1 is gone</source>
        <translation>%1 a disparu</translation>
    </message>
    <message>
        <source>%1 is inactive</source>
        <translation>%1 est inactif</translation>
    </message>
    <message>
        <source>%1 has paused composing</source>
        <translation>%1 a arrêté d&apos;écrire</translation>
    </message>
</context>
<context>
    <name>IdentitiesComboBox</name>
    <message>
        <source>Create a new identity...</source>
        <translation>Créer une nouvelle identité...</translation>
    </message>
    <message>
        <source>New Identity</source>
        <translation>Nouvelle identité</translation>
    </message>
    <message>
        <source>Please enter the name for the new identity:</source>
        <translation>Veuillez saisir le nom de la nouvelle identité :</translation>
    </message>
</context>
<context>
    <name>IdentityManager</name>
    <message>
        <source>Friends</source>
        <translation>Amis</translation>
    </message>
    <message>
        <source>Work</source>
        <translation>Le travail</translation>
    </message>
    <message>
        <source>School</source>
        <translation>École</translation>
    </message>
</context>
<context>
    <name>ImageDialog</name>
    <message>
        <source>Images</source>
        <translation>Les images</translation>
    </message>
</context>
<context>
    <name>KaduDialog</name>
    <message>
        <source>Ok</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KaduMenu</name>
    <message>
        <source>More Actions...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KaduWebView</name>
    <message>
        <source>Save image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <source>File already exists. Overwrite?</source>
        <translation>le fichier existe déjà. L&apos;écraser ?</translation>
    </message>
    <message>
        <source>Cannot save image: %1</source>
        <translation>Impossible d&apos;enregistrer l&apos;image : %1</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <source>Copy Image</source>
        <translation>Copier l&apos;image</translation>
    </message>
    <message>
        <source>Save Image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Cannot save this image</source>
        <translation>Impossible d&apos;enregistrer cette image</translation>
    </message>
    <message>
        <source>Cannot save image</source>
        <translation>Impossible d&apos;enregistrer l&apos;image</translation>
    </message>
    <message>
        <source>Copy Link Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Run Inspector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Overwrite</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KaduWindow</name>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>&amp;Buddies</source>
        <translation>&amp;Amis</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
</context>
<context>
    <name>KaduWindowActions</name>
    <message>
        <source>Add Buddy...</source>
        <translation>Ajouter un ami...</translation>
    </message>
    <message>
        <source>View Buddy Properties</source>
        <translation>Voir les propriétés de l&apos;ami</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Add Group...</source>
        <translation>Ajouter un groupe...</translation>
    </message>
    <message>
        <source>Search for Buddy...</source>
        <translation>Rechercher un ami...</translation>
    </message>
    <message>
        <source>A&amp;bout Kadu</source>
        <translation>À &amp;propos de Kadu</translation>
    </message>
    <message>
        <source>Show Information Panel</source>
        <translation>Afficher le panneau d&apos;information</translation>
    </message>
    <message>
        <source>Show Blocked Buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Copy Description</source>
        <translation>Copier la description</translation>
    </message>
    <message>
        <source>Copy Personal Info</source>
        <translation>Copier les informations personnelles</translation>
    </message>
    <message>
        <source>Send E-Mail</source>
        <translation>Envoyer un email</translation>
    </message>
    <message>
        <source>Search in Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show Offline Buddies</source>
        <translation>Afficher les amis hors ligne</translation>
    </message>
    <message>
        <source>Only Show Buddies with Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show Descriptions</source>
        <translation>Afficher les descriptions</translation>
    </message>
    <message>
        <source>Only Show Online Buddies and Buddies with Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Merge Buddies...</source>
        <translation>Fusionner les amis...</translation>
    </message>
    <message>
        <source>Show Myself Buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View Chat Properties</source>
        <translation>Voir les propriétés de la conversation</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Your Accounts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Multilogons</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Getting H&amp;elp</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Report a Bug</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Get Involved</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Translate Kadu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open Description Link in Browser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose which buddy would you like to merge with &lt;i&gt;%1&lt;/i&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Merge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please enter the name for the new group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add Group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Contact:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>First name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Last name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mobile:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete Chat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete Buddy</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>LeaveChatAction</name>
    <message>
        <source>Leave</source>
        <translation>Interrompre</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>All messages received in this conference will be ignored
from now on. Are you sure you want to leave this conference?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Leave conference</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>LineEditWithClearButton</name>
    <message>
        <source>Clear this field</source>
        <translation>Effacer ce champ</translation>
    </message>
</context>
<context>
    <name>MainConfigurationWindow</name>
    <message>
        <source>None</source>
        <translation>Rien</translation>
    </message>
    <message>
        <source>Kadu configuration</source>
        <translation>Configuration de Kadu</translation>
    </message>
    <message>
        <source>Advanced chat&apos;s look configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open icon theme archive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>XZ archive (*.tar.xz)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Create new toolbar</source>
        <translation>Créer une nouvelle barre d&apos;outils</translation>
    </message>
</context>
<context>
    <name>MergeBuddiesDialogWidget</name>
    <message>
        <source>Merge Buddies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buddy to merge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source> - Select buddy - </source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MessageNotification</name>
    <message>
        <source>Chat with &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Discuter avec &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>New message from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Nouveau message de &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>New chat</source>
        <translation>Nouvelle conversation</translation>
    </message>
    <message>
        <source>New message</source>
        <translation>Nouveau message</translation>
    </message>
</context>
<context>
    <name>MultilogonModel</name>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Ip</source>
        <translation>Ip</translation>
    </message>
    <message>
        <source>Logon time</source>
        <translation>Durée de connexion</translation>
    </message>
</context>
<context>
    <name>MultilogonNotification</name>
    <message>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <source>Disconnect session</source>
        <translation>Déconnecter la session</translation>
    </message>
</context>
<context>
    <name>MultilogonWindow</name>
    <message>
        <source>Multilogon window</source>
        <translation>Fenêtre d&apos;identification multiple</translation>
    </message>
    <message>
        <source>Account:</source>
        <translation>Compte :</translation>
    </message>
    <message>
        <source>Disconnect session</source>
        <translation>Déconnecter la session</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>NewFileTransferNotification</name>
    <message>
        <source>Incoming transfer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>User &lt;b&gt;%1&lt;/b&gt; wants to send you a file &lt;b&gt;%2&lt;/b&gt;&lt;br/&gt;of size &lt;b&gt;%3&lt;/b&gt; using account &lt;b&gt;%4&lt;/b&gt;.&lt;br/&gt;Accept transfer?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>User &lt;b&gt;%1&lt;/b&gt; wants to send you a file &lt;b/&gt;%2&lt;/b&gt;&lt;br/&gt;of size &lt;b&gt;%3&lt;/b&gt; using account &lt;b&gt;%4&lt;/b&gt;.&lt;br/&gt;This is probably a next part of &lt;b&gt;%5&lt;/b&gt;&lt;br/&gt;What should I do?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <source>Save file under new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ignore transfer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <source>Reject</source>
        <translation>Refuser</translation>
    </message>
</context>
<context>
    <name>NotificationManager</name>
    <message>
        <source>Unable to find notifier for %1 event</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
</context>
<context>
    <name>NotificationService</name>
    <message>
        <source>Notify About Buddy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Silent Mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>NotifyConfigurationUiHandler</name>
    <message>
        <source>Use custom settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>NotifyTreeWidget</name>
    <message>
        <source>Event</source>
        <translation>Événement</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
</context>
<context>
    <name>OpenChatWith</name>
    <message>
        <source>Open chat with...</source>
        <translation>Lancer une conversation avec ...</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>User name:</source>
        <translation>Nom d&apos;utilisateur :</translation>
    </message>
</context>
<context>
    <name>PasswordDialogWidget</name>
    <message>
        <source>Incorrect password</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Store this password</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PathListEdit</name>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
</context>
<context>
    <name>PathListEditWindow</name>
    <message>
        <source>Select paths</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation>Choisir un répertoire</translation>
    </message>
</context>
<context>
    <name>Plugin</name>
    <message>
        <source>Cannot find required object in module %1.
Maybe it&apos;s not Kadu-compatible plugin.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Module initialization routine for %1 failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot load %1 plugin library:
%2</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PluginErrorDialog</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Try to load this plugin on next Kadu run</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PluginListWidgetItemDelegate</name>
    <message>
        <source>Plugin name: %1</source>
        <translation>Nom de l&apos;extension : %1</translation>
    </message>
    <message>
        <source>Author: %1</source>
        <translation>Auteur : %1</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation>Version : %1</translation>
    </message>
    <message>
        <source>Description: %1</source>
        <translation>Description : %1</translation>
    </message>
    <message>
        <source>Dependencies: %1</source>
        <translation>Dependences : %1</translation>
    </message>
    <message>
        <source>Conflicts: %1</source>
        <translation>Conflits : %1</translation>
    </message>
    <message>
        <source>Plugin information</source>
        <translation>Informations sur l&apos;extension</translation>
    </message>
</context>
<context>
    <name>PluginsManager</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Required plugin %1 was not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Plugin %1 conflicts with: %2</source>
        <translation>L&apos;extension %1 est en confit avec : %2</translation>
    </message>
    <message>
        <source>Plugin %1 cannot be deactivated because it is being used by the following plugins:%2</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ProgressWindow</name>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Progress:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show details &gt;&gt;&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ProtocolsComboBox</name>
    <message>
        <source> - Select network - </source>
        <translation> - Sélectionner un réseau - </translation>
    </message>
</context>
<context>
    <name>ProxyComboBox</name>
    <message>
        <source> - No proxy - </source>
        <translation> - Aucun proxy - </translation>
    </message>
    <message>
        <source>Edit proxy configuration...</source>
        <translation>Modifier la configuration proxy...</translation>
    </message>
    <message>
        <source> - Use Default Proxy - </source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ProxyEditWindow</name>
    <message>
        <source>Proxy Configuration</source>
        <translation>Configuration proxy</translation>
    </message>
    <message>
        <source>Add new proxy</source>
        <translation>Ajouter un nouveau proxy</translation>
    </message>
    <message>
        <source>HTTP CONNECT method</source>
        <translation>Méthode HTTP CONNECT</translation>
    </message>
    <message>
        <source>SOCKS Version 5</source>
        <translation>SOCKS Version 5</translation>
    </message>
    <message>
        <source>HTTP Polling</source>
        <translation>HTTP Polling</translation>
    </message>
    <message>
        <source>Type (for Jabber)</source>
        <translation>Type (pour Jabber)</translation>
    </message>
    <message>
        <source>Polling URL</source>
        <translation>Polling URL</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Confirm proxy removal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Are you sure do you want to remove this proxy?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remove proxy</source>
        <translation>Supprimer le proxy</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <source>You have unsaved changes in current proxy.&lt;br /&gt;Do you want to save them?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You have unsaved changes in current proxy.&lt;br /&gt;This data is invalid, so you will loose all changes.&lt;br /&gt;Do you want to go back to edit them?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentChatsAction</name>
    <message>
        <source>Recent Chats</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentChatsMenu</name>
    <message>
        <source>Recent chats</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <source>Find:</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Uin</source>
        <translation>Uin</translation>
    </message>
    <message>
        <source>&amp;Personal data</source>
        <translation>Données &amp;personnelles</translation>
    </message>
    <message>
        <source>&amp;Uin number</source>
        <translation>Numéro &amp;Uin</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation>Recherche en cours...</translation>
    </message>
    <message>
        <source>Done searching</source>
        <translation>Recherche terminée</translation>
    </message>
    <message>
        <source>Search User in Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Search Criteria</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Search for this uin exclusively</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Search using the personal data typed above (name, nickname, etc.)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Uin Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Uin:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Personal Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Nick:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Gender:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>First name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Last name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Birth year from:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>to:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>City:</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Search only for active users</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>First Name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Birth Year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ready</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>To be able to search you have to set up an account first.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>We don&apos;t offer contacts search feature for your network yet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot search contacts in offline mode.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>There were no results of your search.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SearchWindowActions</name>
    <message>
        <source>&amp;Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&amp;Next results</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear results</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add selected user</source>
        <translation>Ajouter l&apos;utilisateur sélectionné</translation>
    </message>
    <message>
        <source>&amp;Chat</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SelectFile</name>
    <message>
        <source>Insert image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Select File</source>
        <translation>Insérer une image</translation>
    </message>
    <message>
        <source>Select audio File</source>
        <translation>Sélectionner un fichier audio</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <source>Audio Files (*.wav *.au *.raw);;All Files (*)</source>
        <translation>Fichiers audio (*.wav *.au *.raw);; Tous les fichiers (*)</translation>
    </message>
    <message>
        <source>Select folder</source>
        <translation>Sélectionner un dossier</translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <source>Select</source>
        <translation>Choisir</translation>
    </message>
</context>
<context>
    <name>StatusActions</name>
    <message>
        <source>Change Status Message...</source>
        <translation>Modifier l&apos;état du message</translation>
    </message>
</context>
<context>
    <name>StatusButton</name>
    <message>
        <source>Identity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>StatusChangedNotification</name>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; changed status to &lt;i&gt;%2&lt;/i&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Status changed</source>
        <translation>État modifié</translation>
    </message>
</context>
<context>
    <name>StatusContainerManager</name>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Change status</source>
        <translation>Modifier l&apos;état</translation>
    </message>
    <message>
        <source>Change account status: %1</source>
        <translation>Modifier l&apos;état du compte : %1</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <source>Select Previously Used Description</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear Descriptions History</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Erase Description</source>
        <translation>Effacer la description</translation>
    </message>
    <message>
        <source>&amp;Set status</source>
        <translation>&amp;Sélectionner l&apos;état</translation>
    </message>
    <message>
        <source>Do you really want to clear the descriptions history?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>do not change</source>
        <translation>ne pas modifier</translation>
    </message>
    <message>
        <source>Clear history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SubscriptionWindow</name>
    <message>
        <source>Allow</source>
        <translation>Autoriser</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <source>Ask For Sharing Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>User &lt;b&gt;%1&lt;/b&gt; wants to add you to his contact list.</source>
        <translation>L&apos;utilisateur &lt;b&gt;%1&lt;/b&gt; souhaite vous ajouter à sa liste de contacts.</translation>
    </message>
    <message>
        <source>Do you want this person to see your status?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Allow and add buddy...</source>
        <translation>Autoriser et ajouter un ami...</translation>
    </message>
</context>
<context>
    <name>TokenWindow</name>
    <message>
        <source>Enter Token Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enter text from the picture:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>Remove this toolbar</source>
        <translation>Supprimer cette barre d&apos;outils</translation>
    </message>
    <message>
        <source>Add new button</source>
        <translation>Ajoutez le nouveau bouton</translation>
    </message>
    <message>
        <source>Add new separator</source>
        <translation>Ajouter un nouveau séparateur</translation>
    </message>
    <message>
        <source>Remove this separator</source>
        <translation>Supprimer ce séparateur</translation>
    </message>
    <message>
        <source>Add new expandable spacer</source>
        <translation>Ajouter un nouveau séparateur extensible</translation>
    </message>
    <message>
        <source>Remove this expandable spacer</source>
        <translation>Supprimer ce séparateur extensible</translation>
    </message>
    <message>
        <source>No items to add found</source>
        <translation>Aucun éléments trouvé à ajouter</translation>
    </message>
    <message>
        <source>Remove this button</source>
        <translation>Supprimer ce bouton</translation>
    </message>
    <message>
        <source>Create new toolbar</source>
        <translation>Créer une nouvelle barre d&apos;outils</translation>
    </message>
    <message>
        <source>Block toolbars</source>
        <translation>Verrouiller les barre d&apos;outils</translation>
    </message>
    <message>
        <source>Text position</source>
        <translation>Position du texte</translation>
    </message>
    <message>
        <source>Icon only</source>
        <translation>Icône seulement</translation>
    </message>
    <message>
        <source>Text only</source>
        <translation>Texte seulement</translation>
    </message>
    <message>
        <source>Text alongside icon</source>
        <translation>Texte à côté de l&apos;icône</translation>
    </message>
    <message>
        <source>Text under icon</source>
        <translation>Texte sous l&apos;icône</translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Do you really want to remove this toolbar?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remove toolbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UpdatesDialog</name>
    <message>
        <source>New version is available. Please update</source>
        <translation>Nouveau version disponible. Veuillez mettre à jour.</translation>
    </message>
    <message>
        <source>A new version &lt;b&gt;%1&lt;/b&gt; of Kadu Instant Messenger is available for download. Please &lt;a href=&apos;download&apos;&gt;download&lt;/a&gt; an installer and upgrade or use your package management system to update Kadu.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>A new version &lt;b&gt;%1&lt;/b&gt; of Kadu Instant Messenger is available for download. Please &lt;a href=&apos;download&apos;&gt;download&lt;/a&gt; an installer and upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Check for updates when Kadu is opened</source>
        <translation>Rechercher les mises à jour à l&apos;ouverture de Kadu</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UrlOpener</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Could not spawn Web browser process. Check if the Web browser is functional</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Could not spawn Mail client process. Check if the Mail client is functional</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WindowNotifierWindow</name>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>YourAccounts</name>
    <message>
        <source>Your accounts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Add existing account</source>
        <translation>Ajouter un compte existant</translation>
    </message>
    <message>
        <source>Create new account</source>
        <translation>Créer un nouveau compte</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;+2&apos;&gt;&lt;b&gt;Create New Account&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;+2&apos;&gt;&lt;b&gt;Créer un nouveau compte&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Create New Account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&lt;font size=&apos;+2&apos;&gt;&lt;b&gt;Add Existing Account&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;+2&apos;&gt;&lt;b&gt;Ajouter un compte existant&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Setup an Existing Account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Choose a network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>IM Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You have unsaved changes in current account.&lt;br /&gt;Do you want to save them?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You have unsaved changes in current account.&lt;br /&gt;Do you want to return to edit or discard changes?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Return to edit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Discard changes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save changes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Discard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid changes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You have invalid changes in current account, which cannot be saved.&lt;br /&gt;Do you want to stay in edit or discard changes?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stay in edit</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>