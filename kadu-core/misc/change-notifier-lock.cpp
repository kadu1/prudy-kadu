/*
 * %kadu copyright begin%
 * Copyright 2013 Rafał Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "misc/change-notifier.h"

#include "change-notifier-lock.h"

ChangeNotifierLock::ChangeNotifierLock(ChangeNotifier *notifier, Mode mode)
		: Notifier(notifier), NotifierMode(mode)
{
	if (Notifier)
		Notifier->block();
}

ChangeNotifierLock::~ChangeNotifierLock()
{
	if (!Notifier)
		return;

	if (ModeForget == NotifierMode)
		Notifier->forget();
	Notifier->unblock();
}

void ChangeNotifierLock::setMode(ChangeNotifierLock::Mode mode)
{
	NotifierMode = mode;
}
